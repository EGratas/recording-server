package models

import play.api.test.PlaySpecification

class SqlUtilsSpec extends PlaySpecification {

  "SqlUtils" should {
    "create an IN clause" in {
      SqlUtils.createInClauseOrTrue("callid", List("c1", "c2", "c3")) shouldEqual "callid IN ('c1','c2','c3')"
    }

    "return TRUE" in {
      SqlUtils.createInClauseOrTrue("callid", List()) shouldEqual "TRUE"
    }

    "return FALSE" in {
      SqlUtils.createInClauseOrFalse("callid", List()) shouldEqual "FALSE"
    }
  }
}
