package models.authentication

import helpers.{ConfigTest, UnitDbTest}
import org.scalatestplus.play.guice.GuiceOneAppPerSuite
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.mvc.Results

trait RightHelperS  extends UnitDbTest with ConfigTest with Results with GuiceOneAppPerSuite {
  def appliConfig(context : String): Map[String, Any] = {
    testConfig ++ Map("configManagement.address" -> "configIP:9000","configManagement.httpContext"->context)
  }
}

class RightsHelperSpec extends RightHelperS {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(appliConfig("")).build()

  val rightsHelper = app.injector.instanceOf(classOf[RightsHelper])


  "RightsHelper with empty context" should {

    "generate URL when httpContext is empty" in {
      rightsHelper.getUrl("test") shouldEqual("http://configIP:9000/api/1.0/rights/user/test")
    }
  }
}

class RightsHelperWithContextSpec extends RightHelperS {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(appliConfig("cfg")).build()

  val rightsHelper = app.injector.instanceOf(classOf[RightsHelper])

  "RightsHelper with context with a path" should {
    "generate URL with inserted context" in {
      rightsHelper.getUrl("test") shouldEqual("http://configIP:9000/cfg/api/1.0/rights/user/test")
    }
  }
}

class RightsHelperWithContextWithASlashSpec extends RightHelperS {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(appliConfig("cfgWith/")).build()

  val rightsHelper = app.injector.instanceOf(classOf[RightsHelper])


  "RightsHelper with context with a path with /" should {
    "generate URL without double /" in {
      rightsHelper.getUrl("test") shouldEqual("http://configIP:9000/cfgWith/api/1.0/rights/user/test")
    }
  }
}
