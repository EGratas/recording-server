package models

import helpers.{AppTest, RecordHelper}
import java.time.LocalDateTime
import org.scalatestplus.mockito.MockitoSugar
import play.api.db.Database
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder

import scala.util.Success


class ChannelEventDaoSpec extends AppTest with MockitoSugar {
  val dbStatsMock: Database = mock[Database]

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[Database].qualifiedWith("stats").to(dbStatsMock))
    .build()

  class Helper() {
    val controller: ChannelEventDao = app.injector.instanceOf(classOf[ChannelEventDao])
  }

  def before = {
    DBUtil.cleanTable("cel")
  }

  "ChannelEventDaoSpec" should {
    "get cels from database" in new Helper {
      val date = LocalDateTime.of(2020,3,26, 13,18,27, 123456000)
      val dateString = "2020-03-26 13:18:27.123456"

      val id1 = RecordHelper.insertCel("LINKED_END", dateString, "James Bond", "1000", "SIP/abcd-000000001", "1584976887.1", "1584976887.1")
      val id2 = RecordHelper.insertCel("LINKED_END", dateString, "Bruce Willis", "1001", "SIP/efgh-000000002", "mds1-1584976887.2", "mds1-1584976887.4")
      val id3 = RecordHelper.insertCel("LINKED_END", dateString, "Bruce Willis", "1001", "SIP/efgh-000000002", "mds1-1584976887.2", "mds1-1584976887.4")


      controller.list(1, 1) shouldEqual Success(List(
        ChannelEventLog(id2.getOrElse(0L).toInt, "LINKED_END", date, "", "Bruce Willis", "1001", "1001", "", "", "", "", "SIP/efgh-000000002", "", "", 0, "", "", "mds1-1584976887.2", "mds1-1584976887.4", "", "", Some(0), None)
      ))
    }
  }
}
