package helpers

import configuration.AuthConfig
import models.DBUtil
import org.joda.time.format.DateTimeFormat
import play.api.Configuration

trait DbTest {
  def testConfig = Map(
    "db.default.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.default.username" -> "asterisk",
    "db.default.password" -> "asterisk",
    "db.stats.url" -> "jdbc:postgresql://localhost/asterisktest",
    "db.stats.username" -> "asterisk",
    "db.stats.password" -> "asterisk",
    "play.http.secret.key" -> "test-secret",
    "play.evolutions.enabled" -> "false",
    "audio.folder" -> "/tmp/sounds",
    "audio.extensions" -> List("wav"),
    "audio.weeksToKeep" -> "12",
    "audio.purgeQueryFetchSize" -> 1500,
    "play.ws.ssl.loose.acceptAnyCertificate" -> true,
    "play.ws.ssl.loose.disableHostnameVerification" -> true,
    "play.http.session.cookieName"->"RECORDINGSERVER_SESSION_TEST",
    "play.http.context" -> "/recording",
    "authentication.token" -> "testToken@uè",
    "play.i18n.langs" -> List("en","fr", "de")
    )

  val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
  implicit val connection = DBUtil.getConnection()
  DBUtil.setupDB("record.xml")
}

trait ConfigTest {

  private val minimalAuthTestConfig = Map(
    "authentication.login.login" -> "testuser",
    "authentication.login.password" -> "testpassword",
    "authentication.token" -> "testToken@uè",
    "authentication.xivo.restapi.users" -> "/api/1.0/users",
    "authentication.xivo.restapi.token" -> "token"
  )

  lazy val testAuthConfig = new AuthConfig(Configuration.from(minimalAuthTestConfig))

}
