package controllers

import akka.stream.Materializer
import date_helpers.DateHelper
import helpers.AppTest
import models._
import org.joda.time.Duration
import org.mockito.Mockito.{when, verify}
import org.scalatestplus.mockito.MockitoSugar
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsArray, JsValue, Json}
import play.api.mvc.AnyContentAsJson
import play.api.test.FakeRequest
import play.api.test.Helpers._
import play.api.mvc.Result

import scala.concurrent.Future
import org.joda.time.DateTime

class HistorySpec extends AppTest with MockitoSugar {
  val callDetailDaoMock: CallDetailDao = mock[CallDetailDao]
  val dateHelper: DateHelper = mock[DateHelper]

  override def fakeApplication(): play.api.Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[CallDetailDao].to(callDetailDaoMock))
    .overrides(bind[DateHelper].to(dateHelper))
    .build()

  val context: Any = testConfig("play.http.context")


  class Helper() {
    val controller: History = app.injector.instanceOf(classOf[History])
  }

  "History controller" should {
    "search the call history" in new Helper {
      implicit val materializer: Materializer = app.materializer
      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val returnList = List(CallDetail(formatter.parseDateTime("2013-01-01 15:30:21"),
        Some(new Duration(361 * 1000)),
        Some("1000"),
        Some("2000"),
        CallStatus.Missed,
        Some("James"),
        Some("Bond"),
        Some("Jason"),
        Some("Bourne")))

      when(callDetailDaoMock.historyByInterface("SIP/fndsjl", 10)).thenReturn(returnList)

      val res: Future[Result] = call(controller.search, req)
      status(res) shouldEqual 200
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2013-01-01 15:30:21",
        "duration" -> "00:06:01",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      )))
    }

  

    "return an error if given the parameters days and size at the same time" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?days=7&size=4")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val Some(res) = route(app,req)
      status(res) shouldEqual 400
    }

    "return an error if given an unknown parameter" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?userid=5")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val Some(res) = route(app,req)
      status(res) shouldEqual 400
    }
  }

  "History Agent controller" should {
    "call the history by agent num for last 10 entries since 7 days " in new Helper {
      implicit val materializer: Materializer = app.materializer
      val agentNum = "1004"
      val defaultSize = 10
      val defaultDays = 7
      val fake7DaysOldDate = "2021-01-12 09:27:45"
      val cd1 = CallDetail(new DateTime(), None, None, None, CallStatus.Answered)

      val req = FakeRequest("POST", s"$context/history/agent")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("agentNum" -> s"${agentNum}"))

      when(dateHelper.dateByNumberOfDays(defaultDays)).thenReturn(fake7DaysOldDate)
      when(callDetailDaoMock.historyByAgentNum(agentNum, defaultSize, fake7DaysOldDate)).thenReturn(List(cd1))

      val res = call(controller.searchAgent(defaultSize, defaultDays), req)

      status(res) shouldEqual 200
      verify(callDetailDaoMock).historyByAgentNum(agentNum, defaultSize, fake7DaysOldDate)
      contentAsJson(res) shouldEqual Json.toJson(List(cd1))
    }

    "return bad request if called with wrong parameter" in new Helper {
      implicit val materializer: Materializer = app.materializer
      val agentNum = "1004"
      val defaultSize = 10
      val defaultDays = 7

      val req = FakeRequest("POST", s"$context/history/agent")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("wrongParam" -> s"${agentNum}"))

      val res = call(controller.searchAgent(defaultSize, defaultDays), req)

      status(res) shouldEqual 400
    }
  }
}
