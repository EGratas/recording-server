package xivo.recording

import akka.actor.ActorSystem
import akka.stream.testkit.scaladsl.TestSink
import helpers.{AppTest, RecordHelper, TestUtils}
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder



class AttachedDataSourceSpec extends AppTest with BeforeAndAfterEach with ScalaFutures {
  implicit val system: ActorSystem = ActorSystem()

  val xivoAudioFolder: String = s"${testConfig("audio.folder")}/xivo"

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  val dbStats = app.injector.instanceOf(classOf[Database])

  override protected def beforeEach(): Unit = {
    TestUtils.cleanDirectory(xivoAudioFolder)
    TestUtils.cleanTables(List("attached_data", "call_data", "call_on_queue", "call_element", "transfers"))
  }

  class Helper() {
    val weeksToKeep: Int = Integer.parseInt(testConfig("audio.weeksToKeep").toString)

    val attachedDataSource = new AttachedDataSource(dbStats)
    val testSink = TestSink.probe[String]
  }


  "A AttachedDataSourceSpec " should {
    "retrieve files to delete with xivo recording expiration date from attached data" in new Helper {
      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)
      val refTime = attachedDataSource.toTimeStamp(weeksToKeep)

      val id1 = "123.456"
      val id2 = "123.555"
      val purgeDate1 = "2016-08-27 10:28:10"
      val purgeDate2 = "2015-08-27 10:28:10"

      val callid1 = s"gw1-$id1"
      val callid2 = s"gw1-$id2"

      RecordHelper.insertRecord(id1, callid1, startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate1))
      RecordHelper.insertRecord(id2, callid2, startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> purgeDate2))

      attachedDataSource.getAttachedDataToPurge(refTime).runWith(testSink)
        .requestNext(callid1)
        .requestNext(callid2)
        .expectComplete()
    }
  }
}
