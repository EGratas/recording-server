package xivo.recording

import akka.Done
import akka.actor.ActorSystem
import akka.stream.scaladsl.{Keep, Sink, Source}
import akka.stream.testkit.scaladsl.TestSink
import helpers.{AppTest, RecordHelper, TestUtils}
import org.joda.time.DateTime
import org.mockito.Matchers.any
import org.mockito.Mockito.{reset, verify, when}
import org.mockito.{ArgumentCaptor, Mockito}
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.mockito.MockitoSugar
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder

import java.io.File
import java.nio.file.{NoSuchFileException, NotDirectoryException}
import java.sql.Timestamp
import scala.concurrent.{ExecutionContext, Future}


class DirVisitorSpec extends AppTest with BeforeAndAfterEach with ScalaFutures with MockitoSugar {
  implicit val system: ActorSystem = ActorSystem()
  implicit val executionContext = ExecutionContext.Implicits.global

  val xivoAudioFolder: String = s"${testConfig("audio.folder")}/xivo"
  val weeksToKeep: Int = Integer.parseInt(testConfig("audio.weeksToKeep").toString)
  val audioExtensions: List[String] = testConfig("audio.extensions").asInstanceOf[List[String]]
  val purgeFetchSize: Int = Integer.parseInt(testConfig("audio.purgeQueryFetchSize").toString)

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  val dbStats = app.injector.instanceOf(classOf[Database])

  class Helper() {
    def getDirVisitor(path: String) = new DirVisitor(path, audioExtensions, weeksToKeep, purgeFetchSize, dbStats)
  }

  override protected def beforeEach(): Unit = {
    TestUtils.cleanDirectory(xivoAudioFolder)
    TestUtils.cleanTables(List("attached_data", "call_data", "call_on_queue", "call_element", "transfers"))
  }

  "A DirVisitor " should {
    "handle if directory is not present" in new Helper {
      val dir_tmp = new File("/tmp/tmp.piePee3m")
      if (dir_tmp.exists) {
        dir_tmp.delete()
      }

      val dirVisitor = getDirVisitor(dir_tmp.getName)
      val sinkUnderTest = Sink.head[String]
      val (_, future) = dirVisitor.dirVisitorStream(dirVisitor.toTimeStamp(weeksToKeep)).toMat(sinkUnderTest)(Keep.both).run()

      future.failed.futureValue shouldBe a[NoSuchFileException]
      assert(future.failed.futureValue.getMessage == "tmp.piePee3m")
    }

    "handle if it is not a directory" in new Helper {
      TestUtils.cleanDirectory(xivoAudioFolder)
      val dummyFile = "dummyfile"
      TestUtils.createFileWithDate(s"$xivoAudioFolder/$dummyFile", new DateTime())

      val dirVisitor = getDirVisitor(s"$xivoAudioFolder/$dummyFile")
      val sinkUnderTest = Sink.head[String]
      val (_, future) = dirVisitor.dirVisitorStream(dirVisitor.toTimeStamp(weeksToKeep)).toMat(sinkUnderTest)(Keep.both).run()

      future.failed.futureValue shouldBe a[NotDirectoryException]
      assert(future.failed.futureValue.getMessage == "/tmp/sounds/xivo/dummyfile")
    }

    "trim file extension" in new Helper {
      TestUtils.cleanDirectory(xivoAudioFolder)
      val dummyFile1 = "dummyfile1.ext1"
      val dummyFile2 = "dummyfile1.ok.ext1"
      val dummyFile3 = "dummyfile1"

      val dirVisitor = getDirVisitor(s"$xivoAudioFolder/$dummyFile1")

      dirVisitor.trimFileExtension(dummyFile1) shouldEqual "dummyfile1"
      dirVisitor.trimFileExtension(dummyFile2) shouldEqual "dummyfile1.ok"
      dirVisitor.trimFileExtension(dummyFile3) shouldEqual "dummyfile1"
    }

    "set purged flag for record in attached data" in new Helper {
      TestUtils.cleanTables(List("attached_data", "call_data"))
      val dummyFile1 = "dummyfile1.ext1"
      val startDate: DateTime = DateTime.now.minusWeeks(weeksToKeep).minusDays(1)
      val endDate: DateTime = startDate.plusMinutes(5)
      val id1 = "123.456"

      val dirVisitor = getDirVisitor(s"$xivoAudioFolder/$dummyFile1")

      RecordHelper.insertRecord(id1, "xivo-123456.789", startDate, Some(endDate), Some("1001"), Some("2001"))

      val testSink = TestSink.probe[(Int, String)]

      Source.single((true, "xivo-123456.789.wav"))
        .via(dirVisitor.setPurgeFlagInDatabase)
        .runWith(testSink)
        .requestNext((1, "xivo-123456.789.wav"))
        .expectComplete()
    }

    "not set purged flag for non-existing record in attached data" in new Helper {
      val dummyFile1 = "dummyfile1.ext1"
      val dirVisitor = getDirVisitor(s"$xivoAudioFolder/$dummyFile1")

      val testSink = TestSink.probe[(Int, String)]

      Source.single((true, "xivo-notexisting")).via(dirVisitor.setPurgeFlagInDatabase).runWith(testSink)
        .requestNext((0, "xivo-notexisting"))
        .expectComplete()
    }

    "set purged flag for record in attached data with xivo recording expiration" in new Helper {
      TestUtils.cleanTables(List("attached_data", "call_data"))
      val dummyFile1 = "dummyfile1.ext1"
      val startDate: DateTime = DateTime.now.minusWeeks(weeksToKeep).minusDays(1)
      val endDate: DateTime = startDate.plusMinutes(5)
      val id1 = "123.456"
      val date1 = "2016-08-27 10:28:10"

      val dirVisitor = getDirVisitor(s"$xivoAudioFolder/$dummyFile1")

      RecordHelper.insertRecord(id1, "xivo-123456.789", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> date1))

      val testSink = TestSink.probe[(Int, String)]

      Source.single((true, "xivo-123456.789.wav")).via(dirVisitor.setPurgeFlagInDatabase).runWith(testSink)
        .requestNext((2, "xivo-123456.789.wav"))
        .expectComplete()
    }

    "delete files older than audio.weeksToKeep days (and keep the newer file)" in new Helper {
      val oldFile = "xivo-123456.789.wav"
      val recentFile = "xivo-456789.123.wav"
      val oldDate = new DateTime().minusWeeks(weeksToKeep).minusDays(1)
      val recentDate = new DateTime().minusWeeks(weeksToKeep).plusDays(1)

      val startDate: DateTime = DateTime.now
      val endDate: DateTime = startDate.plusMinutes(5)

      val id1 = "123.456"
      val date1: String = formatter.print(oldDate)

      RecordHelper.insertRecord(id1, "xivo-123456.789", startDate, Some(endDate), Some("1001"), Some("2001"), Map("xivo_recording_expiration" -> date1))

      TestUtils.createFileWithDate(s"$xivoAudioFolder/$oldFile", oldDate)
      TestUtils.createFileWithDate(s"$xivoAudioFolder/$recentFile", recentDate)

      val dirVisitor = getDirVisitor(xivoAudioFolder)
      val done: Future[Done] = dirVisitor.dirVisitorStream(dirVisitor.toTimeStamp(weeksToKeep)).runWith(Sink.ignore)

      done.futureValue shouldBe Done
      new File(xivoAudioFolder).listFiles().map(f => f.getName) shouldEqual Array(recentFile)
    }

    "delete files older than audio.weeksToKeep days (and keep the newer file) without recording expiration key" in new Helper {
      TestUtils.cleanDirectory(xivoAudioFolder)
      TestUtils.cleanTables(List("attached_data", "call_data"))

      val oldFile = "xivo-123456.789.wav"
      val recentFile = "xivo-456789.123.wav"
      val startDate: DateTime = DateTime.now.minusWeeks(weeksToKeep).minusDays(1)
      val endDate: DateTime = startDate.plusMinutes(5)

      val id1 = "123.456"

      RecordHelper.insertRecord(id1, "xivo-123456.789", startDate, Some(endDate), Some("1001"), Some("2001"))

      TestUtils.createFileWithDate(s"$xivoAudioFolder/$oldFile", new DateTime().minusWeeks(weeksToKeep).minusDays(1))
      TestUtils.createFileWithDate(s"$xivoAudioFolder/$recentFile", new DateTime().minusWeeks(weeksToKeep).plusDays(1))

      val dirVisitor = getDirVisitor(xivoAudioFolder)
      val done: Future[Done] = dirVisitor.dirVisitorStream(dirVisitor.toTimeStamp(weeksToKeep)).runWith(Sink.ignore)

      done.futureValue shouldBe Done

      new File(xivoAudioFolder).listFiles().map(f => f.getName) shouldEqual Array(recentFile)
    }

    "NOT delete files if nothing retrieved from the database" in new Helper {
      TestUtils.cleanDirectory(xivoAudioFolder)
      val oldFile = "xivo-123456.789"
      val recentFile = "xivo-456789.123"
      TestUtils.createFileWithDate(s"$xivoAudioFolder/$oldFile", new DateTime().minusWeeks(weeksToKeep).minusDays(1))
      TestUtils.createFileWithDate(s"$xivoAudioFolder/$recentFile", new DateTime().minusWeeks(weeksToKeep).plusDays(1))

      val dirVisitor = getDirVisitor(xivoAudioFolder)
      val done: Future[Done] = dirVisitor.dirVisitorStream(dirVisitor.toTimeStamp(weeksToKeep)).runWith(Sink.ignore)

      done.futureValue shouldBe Done

      new File(xivoAudioFolder).listFiles().map(f => f.getName) should contain only(oldFile, recentFile)
    }

    "call dirvisitor with different reference time" in {
      val attachedDataSourceMock = mock[AttachedDataSource]
      val dirVisitor = new DirVisitor(xivoAudioFolder, audioExtensions, weeksToKeep, purgeFetchSize, dbStats) {
        override val attachedDataSource: AttachedDataSource = attachedDataSourceMock
      }

      val firstEventCaptor = ArgumentCaptor.forClass(classOf[Timestamp])
      val secondEventCaptor = ArgumentCaptor.forClass(classOf[Timestamp])

      when(attachedDataSourceMock.getAttachedDataToPurge(any(), any())).thenReturn(Source.empty)

      dirVisitor.run()
      verify(attachedDataSourceMock, Mockito.timeout(250)).getAttachedDataToPurge(firstEventCaptor.capture(), any())

      reset(attachedDataSourceMock)

      dirVisitor.run()
      verify(attachedDataSourceMock, Mockito.timeout(250)).getAttachedDataToPurge(secondEventCaptor.capture(), any())

      firstEventCaptor.getValue should not equal secondEventCaptor.getValue
    }
  }
}
