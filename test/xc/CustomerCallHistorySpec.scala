package xc

import helpers.{AppTest, RecordHelper}
import models._
import org.joda.time.{DateTime, Period}
import play.api.inject.guice.GuiceApplicationBuilder

class CustomerCallHistorySpec extends AppTest {
  val src_num = "123"
  val filters = List(DynamicFilter("src_num", Some(OperatorEq), Some(src_num)))
  val tables = List("attached_data", "call_data", "call_on_queue", "agentfeatures", "queuefeatures")

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  implicit val paginator = Paginator(0, 20)
  tables.foreach(DBUtil.cleanTable)

  def cleanAndFillDB(): Unit = {
    for (table <- tables)
      DBUtil.cleanTable(table)

    RecordHelper.insertAgent("James", "Bond","8000",1)
    RecordHelper.insertQueue("test","3000",Some("Test Queue"))

    RecordHelper.insertCallData("1", formatter.parseDateTime("2017-01-01 15:30:20").toDate,
      Some(formatter.parseDateTime("2017-01-01 15:35:20").toDate), Some(src_num), Some("3000"))
    RecordHelper.insertCallOnQueue("1","8000","test",QueueCallStatus.Answered)

    RecordHelper.insertCallData("2", formatter.parseDateTime("2017-01-01 16:30:00").toDate,
      Some(formatter.parseDateTime("2017-01-01 16:35:00").toDate),Some(src_num), Some("3000"))
    RecordHelper.insertCallOnQueue("2","8000","test",QueueCallStatus.Answered)
  }

  val customerCallDetailDao = app.injector.instanceOf(classOf[CustomerCallDetailDao])

  "CustomerCallHistory controller" should {

    "search the customer call history by src_num" in  {
      cleanAndFillDB()

      customerCallDetailDao.findHistoryByCustomer(filters, 10).list should contain allOf (
        CustomerCallDetail(formatter.parseDateTime("2017-01-01 16:30:00"),
          Some(new Period(0, 5, 0, 0)),
          Some(new Period(0, 0, 0, 0)),
          Some("James Bond"), Some("8000"), Some("Test Queue"), Some("3000"), CallStatus.Answered),
        CustomerCallDetail(formatter.parseDateTime("2017-01-01 15:30:20"),
          Some(new Period(0, 5, 0, 0)),
          Some(new Period(0, 0, 0, 0)),
          Some("James Bond"), Some("8000"), Some("Test Queue"), Some("3000"), CallStatus.Answered))

      customerCallDetailDao.findHistoryByCustomer(filters, 10).total shouldEqual 2
    }

    "limit the response to the given size but keep total" in  {
      cleanAndFillDB()
      customerCallDetailDao.findHistoryByCustomer(filters, 1).list should contain (
        CustomerCallDetail(formatter.parseDateTime("2017-01-01 16:30:00"), Some(new Period(0, 5, 0, 0)), Some(new Period(0, 0, 0, 0)),
          Some("James Bond"), Some("8000"), Some("Test Queue"), Some("3000"), CallStatus.Answered)
      )
      customerCallDetailDao.findHistoryByCustomer(filters, 1).total shouldEqual 2
    }

    "force CallStatus to be ongoing if no status yet in DB" in  {
      for (table <- tables)
        DBUtil.cleanTable(table)

      RecordHelper.insertAgent("Jason", "Bourne", "8001", 1)
      RecordHelper.insertQueue("test", "3000", Some("Test Queue"))

      RecordHelper.insertCallData("1", formatter.parseDateTime("2017-01-01 17:30:00").toDate,
        Some(formatter.parseDateTime("2017-01-01 17:35:00").toDate), Some(src_num), Some("3000"))
      RecordHelper.insertCallOnQueue("1", "8001", "test", null)

      customerCallDetailDao.findHistoryByCustomer(filters, 1).list should contain(
        CustomerCallDetail(formatter.parseDateTime("2017-01-01 17:30:00"), Some(new Period(0, 5, 0, 0)), Some(new Period(0, 0, 0, 0)),
          Some("Jason Bourne"), Some("8001"), Some("Test Queue"), Some("3000"), CallStatus.Ongoing)
      )
      customerCallDetailDao.findHistoryByCustomer(filters, 1).total shouldEqual 1
    }

    "set duration from now if CallStatus doesn't have end time yet" in  {
      for (table <- tables)
        DBUtil.cleanTable(table)

      RecordHelper.insertAgent("Jason", "Bourne", "8001", 1)
      RecordHelper.insertQueue("test", "3000", Some("Test Queue"))

      RecordHelper.insertCallData("1", new DateTime().minusSeconds(5).toDate,
        None, Some(src_num), Some("3000"))
      RecordHelper.insertCallOnQueue("1", "8001", "test", null)

      customerCallDetailDao.findHistoryByCustomer(filters, 1).list.head.duration.get.getSeconds should be >=(5)
    }
  }
}
