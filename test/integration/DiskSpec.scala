package integration

import akka.stream.Materializer
import controllers.{Disk, Secured}
import helpers.{AppTest, TestUtils}
import models.Partition
import models.authentication.{AdminRight, RightsHelper, SupervisorRight}
import play.api.Configuration
import play.api.cache.SyncCacheApi
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.{call, _}
import play.api.test.{FakeRequest, Helpers}

class DiskSpec extends AppTest {

  val myConfig = Map(
    "audio.folder" -> "/tmp"
  )

  val myCfg = Configuration.from(myConfig)

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  implicit val materializer: Materializer = app.materializer

  val rightsHelper = app.injector.instanceOf(classOf[RightsHelper])
  val secured = app.injector.instanceOf(classOf[Secured])

  val cc = Helpers.stubControllerComponents(bodyParser = stubPlayBodyParsers.anyContent)
  val diskController = new Disk(testAuthConfig, myCfg, rightsHelper, cc, secured)
  val cache = app.injector.instanceOf(classOf[SyncCacheApi])

  "The disk application" should {
    "return the state of the audio directory in JSON" in  {
      cache.set("test", AdminRight())
      TestUtils.cleanDirectory(testConfig("audio.folder").toString)

      val req = FakeRequest("GET", "/partitions").withSession("username" -> "test")
      val result = call(diskController.listPartitions, req)

      (contentAsJson(result) \\ "name") (0).as[String] should startWith("RecordingsPartition")
      (contentAsJson(result) \\ "mountPoint") (0).as[String] should equal(myConfig("audio.folder"))
      (contentAsJson(result) \\ "freeSpace") (0).as[Long] should be >= 0L
      (contentAsJson(result) \\ "totalSpace") (0).as[Long] should be > ((contentAsJson(result) \\ "freeSpace") (0).as[Long])
    }

    "compute disk ration usage" in {
      val p = Partition("testPartition", mountPoint = "Here", freeSpace = 1000L, totalSpace = 3000L)
      diskController.diskUsage(p) shouldEqual 0.6667
    }

    "allow non-admin user to get disk space" in  {
      cache.set("test", SupervisorRight(List[Int](), List[Int](), List[Int]()))
      TestUtils.cleanDirectory(testConfig("audio.folder").toString)

      val req = FakeRequest("GET", "/partitions").withSession("username" -> "test")
      val result = call(diskController.listPartitions, req)

      status(result) should equal(OK)
    }


  }
}
