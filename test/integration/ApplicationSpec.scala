package integration

import helpers.{AppTest, RecordHelper}
import models.authentication.{AdminRight, SupervisorRight}
import models.{AttachedData, DBUtil, Paginator, Record}
import play.api.cache.SyncCacheApi
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsArray, JsString, Json}
import play.api.test.FakeRequest
import play.api.test.Helpers.{status, _}

import java.util.Date

class ApplicationSpec extends AppTest {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  val context = testConfig("play.http.context")
  val token = testConfig("authentication.token").toString
  val cache = app.injector.instanceOf(classOf[SyncCacheApi])

  val dbTest = app.injector.instanceOf(classOf[Database])

  "Recording server" should {

    "search records and reply in JSON" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
                                                      .withJsonBody(Json.obj("callee" -> "2000", "caller" -> "1000", "agent" -> "3000", "queue" -> "thequeue",
                                                                             "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
        RecordHelper.insertTransfers("1503395751.4", "1503395831.14")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
                                                                              "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records and reply without agent in JSON - agent num in call on queue" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("callee" -> "2000", "caller" -> "1000", "agent" -> "3000", "queue" -> "thequeue",
          "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List()
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records and reply without agent in JSON - agent num not in call on queue" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("agent" -> "2000", "queue" -> "thequeue",
          "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "2000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"), dstAgent = Some("2000"))
        RecordHelper.insertCallOnQueue("123", "", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List()
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search specific agent in records and reply in JSON" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("agent" -> "4000","start" -> "2013-01-01 15:30:00", "key" -> "recording"))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertAgent("Brice", "deNice", "4000", 0)
        RecordHelper.insertQueue("thequeue1", "5000")
        RecordHelper.insertQueue("thequeue2", "6000")
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue1")
        RecordHelper.insertCallOnQueue("456", "4000", "thequeue2")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"), Map("key" -> "value"))
        RecordHelper.insertTransfers("123", "456")
        RecordHelper.insertTransfers("123", "123")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"

      records.as[JsArray].value.size shouldEqual 2
      (records(1) \ "id").as[String] shouldEqual "gw1-123"
      (records(1) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(1) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1001"
      (records(0) \ "dst_num").as[String] shouldEqual "2001"
      (records(1) \ "src_num").as[String] shouldEqual "1000"
      (records(1) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Brice deNice (4000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue2 (6000)")
      (records(1) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)", "Brice deNice (4000)")
      (records(1) \ "queue").as[List[String]] shouldEqual List("thequeue1 (5000)", "thequeue2 (6000)")
      (records(1) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search in records and reply in JSON with multiple queues and agents" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("start" -> "2013-01-01 15:30:00", "key" -> "recording" ))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertAgent("Brice", "deNice", "4000", 0)
        RecordHelper.insertQueue("thequeue1", "5000")
        RecordHelper.insertQueue("thequeue2", "6000")
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue1")
        RecordHelper.insertCallOnQueue("456", "4000", "thequeue2")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"), Map("key" -> "value"))
        RecordHelper.insertTransfers("123", "456")
        RecordHelper.insertTransfers("123", "123")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 2
      (records(1) \ "id").as[String] shouldEqual "gw1-123"
      (records(1) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(1) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1001"
      (records(0) \ "dst_num").as[String] shouldEqual "2001"
      (records(1) \ "src_num").as[String] shouldEqual "1000"
      (records(1) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Brice deNice (4000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue2 (6000)")
      (records(1) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)", "Brice deNice (4000)")
      (records(1) \ "queue").as[List[String]] shouldEqual List("thequeue1 (5000)", "thequeue2 (6000)")
      (records(1) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records by callid and reply in JSON" in {
      cache.set("test", AdminRight())
      val req = FakeRequest("POST", s"$context/records/callid_search?callid=123").withSession(("username", "test"))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
        RecordHelper.insertTransfers("1503395751.4", "1503395831.14")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
        (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records and reply in JSON when using token" in {
      val req = FakeRequest("POST", s"$context/records/search")
        .withJsonBody(Json.obj("callee" -> "2000", "caller" -> "1000", "agent" -> "3000", "queue" -> "thequeue",
          "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
        .withHeaders("X-Auth-Token" -> token)

      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        RecordHelper.insertQueue("thequeue", "5000")
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
        RecordHelper.insertTransfers("1503395751.4", "1503395831.14")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "search records and reply forbiden with incorrect token" in {
      val req = FakeRequest("POST", s"$context/records/search")
        .withJsonBody(Json.obj("callee" -> "2000", "caller" -> "1000", "agent" -> "3000", "queue" -> "thequeue",
          "start" -> "2013-01-01 15:30:00", "end" -> "2013-01-01 15:40:00", "key" -> "recording" ))
        .withHeaders("X-Auth-Token" -> "wrong")


      val Some(res) = route(app,req)
      status(res) shouldEqual FORBIDDEN
    }

    "filter records according to the user's rights" in {
      val req = FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("start" -> "2013-01-01 15:30:00", "key" -> "recording" ))
      dbTest.withConnection(implicit connection => {
        List("agentfeatures", "queuefeatures", "call_data", "call_on_queue", "attached_data", "transfers").foreach(DBUtil.cleanTable)
        RecordHelper.insertAgent("Pierre", "Dupond", "3000", 0)
        val queueId1 = RecordHelper.insertQueue("thequeue", "5000").get
        cache.set("test", SupervisorRight(List(queueId1.toInt), List(), List()))
        RecordHelper.insertRecord("123", "gw1-123", formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), Some("1000"), Some("2000"), Map("key" -> "value"))
        RecordHelper.insertCallOnQueue("123", "3000", "thequeue")
        RecordHelper.insertRecord("456", "gw1-456", formatter.parseDateTime("2013-01-01 15:45:21"), Some(formatter.parseDateTime("2013-01-01 15:58:22")), Some("1001"), Some("2001"))
        RecordHelper.insertCallOnQueue("456", "3000", "otherqueue")
        RecordHelper.insertTransfers("1503395751.4", "1503395831.14")
      })
      val Some(res) = route(app,req)
      val json = Json.parse(contentAsString(res))
      val records = json \ "records"
      records.as[JsArray].value.size shouldEqual 1
      (records(0) \ "id").as[String] shouldEqual "gw1-123"
      (records(0) \ "start").as[String] shouldEqual "2013-01-01 15:30:21"
      (records(0) \ "duration").as[String] shouldEqual "00:06:01"
      (records(0) \ "src_num").as[String] shouldEqual "1000"
      (records(0) \ "dst_num").as[String] shouldEqual "2000"
      (records(0) \ "agent").as[List[String]] shouldEqual List("Pierre Dupond (3000)")
      (records(0) \ "queue").as[List[String]] shouldEqual List("thequeue (5000)")
      (records(0) \ "attached_data").as[Map[String, String]] shouldEqual Map("recording" -> "gw1-123",
        "key" -> "value")
      (json \ "hasNext").asOpt[Boolean] shouldEqual Some(false)
    }

    "validate the search criteria" in {
      cache.set("test", AdminRight())
      val res = route(app,FakeRequest("POST", s"$context/records/search").withSession(("username", "test"))
        .withJsonBody(Json.obj("value" -> "the value" ))).get

      status(res) shouldEqual BAD_REQUEST
      contentAsJson(res) shouldEqual JsString("key is mandatory when searching a value")
    }

    "add a list of attached data to a call data" in {
      val idCallData = RecordHelper.insertCallData("123456.789", new Date, None).get
      val json = Json.toJson(List(
        Json.obj("key" -> "test1", "value" -> "value1"),
        Json.obj("key" -> "test2", "value" -> "value2")
      ))

      val res = route(app,FakeRequest("POST", s"$context/call_data/$idCallData/attached_data").withSession("username" -> "test")
      .withJsonBody(json)).get

      status(res) shouldEqual CREATED
      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456.789", AdminRight()).map(_.attachedData) shouldEqual List(Map("test1" -> "value1", "test2" -> "value2"))
    }

    "add attached data with call reason" in {
      RecordHelper.insertCallData("123456.000", new Date, None).get
      val json = Json.toJson(Json.obj("key" -> AttachedData.PurgeKey, "value" -> "2018-08-13 13:21:04"))
      val callDataCallId = "123456.000"

      val res = route(app,FakeRequest("POST", s"$context/call_data/$callDataCallId/attached_data_by_uniqueId").withSession("username" -> "test")
        .withJsonBody(json)).get

      status(res) shouldEqual CREATED
      implicit val paginator = Paginator(0, 1)
      Record.searchByCallid("123456.000", AdminRight()).map(_.attachedData) shouldEqual List(Map(AttachedData.PurgeKey -> "2018-08-13 13:21:04"))
    }
  }
}
