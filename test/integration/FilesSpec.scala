package integration

import java.io.{File, PrintWriter}
import akka.stream.Materializer
import controllers.{Files, Secured}
import helpers.{AppTest, RecordHelper, TestUtils}
import models.CsvLoggerHelper
import models.authentication.{AdminRight, RightsHelper, SupervisorRight}
import org.joda.time.DateTime
import org.mockito.Mockito._
import org.scalatest.BeforeAndAfterEach
import org.scalatestplus.mockito.MockitoSugar
import play.api.Configuration
import play.api.cache.SyncCacheApi
import play.api.db.Database
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.test.Helpers.{status, _}
import play.api.test.{FakeRequest, Helpers}

class FilesSpec extends AppTest with MockitoSugar with BeforeAndAfterEach {

  val accessLogPath = "/tmp/recording-server"
  val tables        = List("attached_data", "call_data")

  override def testConfig = super.testConfig ++ Map(
    "audio.folder"          -> "/tmp",
    "audio.extensions"      -> List("wav", "truc"),
    "audio.fileFallbackUrl" -> "http://someserver?id=%s"
  )

  override def afterEach() = {
    TestUtils.cleanTables(List("attached_data", "call_data"))

    val f1 = new File("/tmp/gw1/gw1-123456.truc")
    if (f1.exists()) f1.delete()

    val f2 = new File(s"$accessLogPath/access.csv")
    if (f2.exists()) f2.delete()

    val f3 = new File(s"$accessLogPath/access.2018-06-27.csv")
    if (f3.exists()) f3.delete()
    ()
  }

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig)
    .build()

  implicit val materializer: Materializer = app.materializer

  val csvHelper    = app.injector.instanceOf(classOf[CsvLoggerHelper])
  val rightsHelper = app.injector.instanceOf(classOf[RightsHelper])
  val db           = app.injector.instanceOf(classOf[Database])
  val cache        = app.injector.instanceOf(classOf[SyncCacheApi])
  val secured      = app.injector.instanceOf(classOf[Secured])
  val cc = Helpers.stubControllerComponents(bodyParser =
    stubPlayBodyParsers.anyContent
  )
  val context = testConfig("play.http.context")

  "The Files controller" should {

    "send file if the file exists and download action" in {
      new File("/tmp/gw1").mkdir()
      val pw = new PrintWriter(new File("/tmp/gw1/gw1-123456.truc"))
      pw.println("do ré mi fa sol la si do")
      pw.close()

      val req =
        FakeRequest("GET", s"$context/records/gw1-123456/audio/download")
          .withSession(("username", "test"))
      val Some(res) = route(app, req)

      status(res) should be(200)
      contentAsBytes(res) should not be empty
    }

    "do not send file back if listen action" in {
      new File("/tmp/gw1").mkdir()
      val pw = new PrintWriter(new File("/tmp/gw1/gw1-123456.truc"))
      pw.println("do ré mi fa sol la si do")
      pw.close()

      val req = FakeRequest("GET", s"$context/records/gw1-123456/audio/listen")
        .withSession(("username", "test"))
      val Some(res) = route(app, req)

      status(res) should be(200)
      contentAsBytes(res) shouldBe empty
    }

    "return not found if the file does not exist and no fallback URL" in {

      val myConfig = Map(
        "audio.folder"          -> "/tmp",
        "audio.extensions"      -> List("wav", "truc"),
        "audio.fileFallbackUrl" -> ""
      )

      val myCfg = Configuration.from(myConfig)

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        csvHelper,
        db,
        cc,
        secured
      )

      cache.set("test", AdminRight())

      new File("/tmp/gw1").mkdir()

      val req =
        FakeRequest("GET", s"$context/records/gw1-123456/audio/download")
          .withSession(("username", "test"))

      val result = call(myFileController.get("gw1-123456"), req)

      status(result) should equal(NOT_FOUND)
    }

    "redirect to the login page if no session is found" in {
      val req =
        FakeRequest("GET", s"$context/records/gw1-123456/audio/download")
      val Some(res) = route(app, req)
      redirectLocation(res).get should include("login")
    }

    "redirect to the fallbackUrl if it is defined and if the file is not found" in {

      val myConfig = Map(
        "audio.folder"          -> "/tmp",
        "audio.extensions"      -> List("wav", "truc"),
        "audio.fileFallbackUrl" -> "http://someserver?id=%s"
      )

      val myCfg = Configuration.from(myConfig)

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        csvHelper,
        db,
        cc,
        secured
      )

      cache.set("test", AdminRight())
      new File("/tmp/gw1").mkdir()

      val req =
        FakeRequest("GET", s"$context/records/gw1-123456/audio/download")
          .withSession(("username", "test"))

      val result = call(myFileController.get("gw1-123456"), req)

      status(result) should equal(SEE_OTHER)

      redirectLocation(result) shouldEqual Some(
        "http://someserver?id=gw1-123456"
      )
    }

    "send Bad Request if action is not valid" in {
      val req =
        FakeRequest("GET", s"$context/records/gw1-123456/audio/dummyAction")
          .withSession(("username", "test"))
      val Some(res) = route(app, req)

      status(res) should be(400)
    }

    "Generate CSV log line format for recording search result access" in {
      val lineResult = "jdoe,gw1-123456.wav,gw1-123456,result"
      val myCfg      = Configuration.from(testConfig)
      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        csvHelper,
        db,
        cc,
        secured
      )

      myFileController.generateCsvLine(
        "jdoe",
        "gw1-123456.wav",
        "gw1-123456",
        "result"
      ) should equal(lineResult)
    }

    "do not write CSV log when callId is not found" in {
      val myCfg         = Configuration.from(testConfig)
      val mockCsvHelper = mock[CsvLoggerHelper]

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        mockCsvHelper,
        db,
        cc,
        secured
      )

      new File("/tmp/gw1").mkdir()
      new File("/tmp/gw1/gw1-123456.truc").createNewFile()
      val req = FakeRequest("GET", s"$context/records/gw1-123456/audio")
        .withSession(("username", "test"))

      call(myFileController.get("gw1-123456"), req)
      verify(mockCsvHelper, never).logCsv("test,gw1-123456,123456,result")
    }

    "write CSV log when any action is done on recording if callId is found" in {
      val myCfg         = Configuration.from(testConfig)
      val mockCsvHelper = mock[CsvLoggerHelper]

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        mockCsvHelper,
        db,
        cc,
        secured
      )

      new File("/tmp/gw1").mkdir()
      new File("/tmp/gw1/gw1-123456.truc").createNewFile()
      val req = FakeRequest("GET", s"$context/records/gw1-123456/audio")
        .withSession(("username", "test"))

      val startDate: DateTime = DateTime.now.minusDays(1)
      val endDate: DateTime   = startDate.plusMinutes(5)

      RecordHelper.insertRecord(
        "123456",
        "gw1-123456",
        startDate,
        Some(endDate),
        Some("1001"),
        Some("2001")
      )

      call(myFileController.get("gw1-123456"), req)
      verify(mockCsvHelper, timeout(100)).logCsv(
        "test,gw1-123456,123456,result"
      )
    }

    "return 401 when calling getAccessLog and not authentified" in {
      val myCfg = Configuration.from(testConfig)
      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        csvHelper,
        db,
        cc,
        secured
      )

      val req    = FakeRequest("GET", s"$context/records/access")
      val result = call(myFileController.getAccessLog, req)

      status(result) should equal(FORBIDDEN)
    }

    "return 401 when calling getAccessLog and not an admin" in {
      val myCfg = Configuration.from(testConfig)
      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        csvHelper,
        db,
        cc,
        secured
      )

      cache.set("test", SupervisorRight(List(), List(), List()))

      val req = FakeRequest("GET", s"$context/records/access").withSession(
        ("username", "test")
      )
      val result = call(myFileController.getAccessLog, req)

      status(result) should equal(FORBIDDEN)
    }

    "return 404 when calling getAccessLog and have no journal" in {
      val myCfg         = Configuration.from(testConfig)
      val mockCsvHelper = mock[CsvLoggerHelper]
      when(mockCsvHelper.filePath).thenReturn("")

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        mockCsvHelper,
        db,
        cc,
        secured
      )
      cache.set("test", AdminRight())

      val req = FakeRequest("GET", s"$context/records/access").withSession(
        ("username", "test")
      )
      val result = call(myFileController.getAccessLog, req)

      status(result) should equal(NOT_FOUND)
    }

    "return 200 when calling getAccessLog when at least one file exists" in {
      val myCfg         = Configuration.from(testConfig)
      val mockCsvHelper = mock[CsvLoggerHelper]
      when(mockCsvHelper.filePath).thenReturn(accessLogPath)

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        mockCsvHelper,
        db,
        cc,
        secured
      )
      cache.set("test", AdminRight())

      new File(accessLogPath).mkdir()
      val pw = new PrintWriter(new File(s"$accessLogPath/access.csv"))
      pw.println("2018-06-28 10:00:00,test,gw1-123456,123456,result")
      pw.close()

      val req = FakeRequest("GET", s"$context/records/access").withSession(
        ("username", "test")
      )
      val result = call(myFileController.getAccessLog, req)

      status(result) should equal(OK)
    }

    "return content of multiple files concatenated with headers when calling getAccessLog with multiple files" in {
      val myCfg         = Configuration.from(testConfig)
      val mockCsvHelper = mock[CsvLoggerHelper]
      when(mockCsvHelper.filePath).thenReturn(accessLogPath)

      val myFileController = new Files(
        testAuthConfig,
        myCfg,
        rightsHelper,
        mockCsvHelper,
        db,
        cc,
        secured
      )
      cache.set("test", AdminRight())

      new File(accessLogPath).mkdir()
      val pw1 =
        new PrintWriter(new File(s"$accessLogPath/access.2018-06-28.csv"))
      pw1.println("2018-06-28 10:00:00,test,gw1-123456,123456,result")
      pw1.println("2018-06-28 10:00:01,test,gw1-123456,123456,listen")
      pw1.close()

      val pw2 =
        new PrintWriter(new File(s"$accessLogPath/access.2018-06-27.csv"))
      pw2.println("2018-06-27 18:00:00,jdoe,gw1-123456,123456,download")
      pw2.close()

      val req = FakeRequest("GET", s"$context/records/access").withSession(
        ("username", "test")
      )
      val result = call(myFileController.getAccessLog, req)

      status(result) should equal(OK)
      contentAsString(result) should equal(
        """date,username,filename,recordingid,action
          |2018-06-27 18:00:00,jdoe,gw1-123456,123456,download
          |2018-06-28 10:00:00,test,gw1-123456,123456,result
          |2018-06-28 10:00:01,test,gw1-123456,123456,listen
          |""".stripMargin
      )

    }
  }
}
