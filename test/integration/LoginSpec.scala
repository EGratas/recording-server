package integration

import akka.stream.Materializer
import controllers.Login
import helpers.AppTest
import models.{AuthenticatedToken, AuthenticatedUser}
import models.authentication.{AdminRight, Right, RightsHelper, SupervisorRight, TeacherRight}
import org.joda.time.{DateTime, DateTimeZone}
import play.api.Configuration
import play.api.cache.SyncCacheApi
import play.api.http.Status._
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.ws.WSClient
import play.api.mvc.Result
import play.api.test.{CSRFTokenHelper, FakeRequest, Helpers}

class MockRightHelper {
  def getFreshUserRights =  AdminRight()
}

class LoginSpec extends AppTest {

  override def fakeApplication() = new GuiceApplicationBuilder()
    .configure(testConfig).build()

  implicit val materializer: Materializer = app.materializer

  val cache = app.injector.instanceOf(classOf[SyncCacheApi])
  val wsClient = app.injector.instanceOf(classOf[WSClient])
  val conf = app.injector.instanceOf(classOf[Configuration])

  val mockRightHelperAdmin = new RightsHelper(conf, wsClient, cache) {
    override def getFreshUserRights(username: String): Option[Right] = {
      Some(AdminRight())
    }
  }

  val mockRightHelperSupervisorWithoutAccess = new RightsHelper(conf, wsClient, cache) {
    override def getFreshUserRights(username: String): Option[Right] = {
      Some(SupervisorRight(List(), List(), List(), recordingAccess = false))
    }
  }

  val mockRightHelperSupervisorWithAccess = new RightsHelper(conf, wsClient, cache) {
    override def getFreshUserRights(username: String): Option[Right] = {
      Some(SupervisorRight(List(), List(), List(), recordingAccess = true))
    }
  }

  // Use of UTC timezone because checkRights function uses isBeforeNow/isAfterNow method
  val utcTimezone = DateTimeZone.UTC

  val mockRightHelperTeacher = new RightsHelper(conf, wsClient, cache) {
    override def getFreshUserRights(username: String): Option[Right] = {
      Some(TeacherRight(List(), List(), List(), new DateTime(utcTimezone).minusDays(2), new DateTime(utcTimezone).plusDays(2)))
    }
  }


  val mockRightHelperTeacherOutdated = new RightsHelper(conf, wsClient, cache) {
    override def getFreshUserRights(username: String): Option[Right] = {
      Some(TeacherRight(List(), List(), List(), new DateTime(utcTimezone).minusDays(2), new DateTime(utcTimezone).minusDays(1)))
    }
  }

  "Login" should {
    "allow access to administrator" in  {

      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperAdmin)

      val user = AuthenticatedUser("test", superAdmin = false)
      val req = FakeRequest("POST", "/login")

      val result: Result = loginController.checkRights(user)(req)
      result.header.status shouldEqual SEE_OTHER
    }

    "allow access to administrator if using token" in  {

      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperAdmin)

      val user = AuthenticatedToken
      val req = FakeRequest("POST", "/login")

      val result: Result = loginController.checkRights(user)(req)
      result.header.status shouldEqual SEE_OTHER
    }

    "forbid access to supervisor without recording access right" in {
      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperSupervisorWithoutAccess)
      loginController.setControllerComponents(Helpers.stubControllerComponents())

      val user = AuthenticatedUser("test", superAdmin = false)
      val reqCsrf = CSRFTokenHelper.addCSRFToken(FakeRequest("POST", "/login"))

      val result: Result = loginController.checkRights(user)(reqCsrf)
      result.header.status shouldEqual BAD_REQUEST
    }

    "allow access to supervisor with recording access right" in {
      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperSupervisorWithAccess)

      val user = AuthenticatedUser("test", superAdmin = false)
      val reqCsrf = CSRFTokenHelper.addCSRFToken(FakeRequest("POST", "/login"))

      val result: Result = loginController.checkRights(user)(reqCsrf)
      result.header.status shouldEqual SEE_OTHER
    }

    "allow access to a teacher within its access period" in {
      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperTeacher)

      val user = AuthenticatedUser("test", superAdmin = false)
      val reqCsrf = CSRFTokenHelper.addCSRFToken(FakeRequest("POST", "/login"))

      val result: Result = loginController.checkRights(user)(reqCsrf)
      result.header.status shouldEqual SEE_OTHER
    }


    "forbid access to a teacher outside its access period" in {
      val loginController = new Login(testAuthConfig, wsClient, mockRightHelperTeacherOutdated)
      loginController.setControllerComponents(Helpers.stubControllerComponents())

      val user = AuthenticatedUser("test", superAdmin = false)
      val reqCsrf = CSRFTokenHelper.addCSRFToken(FakeRequest("POST", "/login"))

      val result: Result = loginController.checkRights(user)(reqCsrf)
      result.header.status shouldEqual BAD_REQUEST
    }
  }
}
