package integration

import anorm.{RowParser, SQL}
import anorm.SqlParser.get
import date_helpers.DateHelper
import helpers.{AppTest, RecordHelper}
import models._
import models.authentication.AdminRight
import org.mockito.Mockito.when
import org.scalatestplus.mockito.MockitoSugar
import play.api.cache.SyncCacheApi
import play.api.db.Database
import play.api.inject.bind
import play.api.inject.guice.GuiceApplicationBuilder
import play.api.libs.json.{JsArray, JsNull, JsValue, Json}
import play.api.mvc.AnyContentAsJson
import play.api.test.FakeRequest
import play.api.test.Helpers._

class HistoryIntegrationSpec extends AppTest with MockitoSugar {
  val dateHelper: DateHelper = mock[DateHelper]

  override def fakeApplication(): play.api.Application = new GuiceApplicationBuilder()
    .configure(testConfig)
    .overrides(bind[DateHelper].to(dateHelper))
    .build()

  val testDb: Database = app.injector.instanceOf(classOf[Database])
  val cache: SyncCacheApi = app.injector.instanceOf(classOf[SyncCacheApi])
  val context: Any = testConfig("play.http.context")

  cache.set("test", AdminRight())
  val tables = List("attached_data", "call_data", "call_on_queue", "agentfeatures", "queuefeatures", "userfeatures", "extensions")

  class Helper() {
    for (table <- tables)
      DBUtil.cleanTable(table)
  }

  "History controller" should {
    "search call elements by interface in the database" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("123", formatter.parseDateTime("2013-01-01 15:30:21").toDate,
        Some(formatter.parseDateTime("2013-01-01 15:36:22").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2013-01-01 15:30:21"), Some(formatter.parseDateTime("2013-01-01 15:36:22")), "SIP/fndsjl")
      val id2: Option[Long] = RecordHelper.insertCallData("456", formatter.parseDateTime("2013-01-01 16:30:21").toDate,
        Some(formatter.parseDateTime("2013-01-01 16:36:22").toDate), Some("1001"), Some("2001"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2013-01-01 16:30:21"), Some(formatter.parseDateTime("2013-01-01 16:36:22")), "SIP/sapanj")

      val Some(res) = route(app, req)

      val json: JsValue = Json.parse(contentAsString(res))
      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2013-01-01 15:30:21",
        "duration" -> "00:06:01",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      )))
    }

    "get the last x calls" in new Helper() {
      val size = 2
      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?size=$size")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      val id4: Option[Long] = RecordHelper.insertCallData("4", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate), Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), "SIP/fndsjl")

      val Some(res) = route(app, req)
      status(res) shouldEqual 200
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2018-06-30 12:00:00",
        "duration" -> "00:05:00",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      ),
        Json.obj(
          "start" -> "2018-06-29 17:00:00",
          "duration" -> "00:05:00",
          "src_num" -> "1000",
          "dst_num" -> "2000",
          "status" -> "missed",
          "src_firstname" -> "James",
          "src_lastname" -> "Bond",
          "dst_firstname" -> "Jason",
          "dst_lastname" -> "Bourne"
        )))
    }

    "return an empty list if there were no calls today" in new Helper() {
      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?days=1")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      when(dateHelper.dateByNumberOfDays(1)).thenReturn("2018-06-29 18:00:00")

      val Some(res) = route(app,req)
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List())
    }

    "get the calls of today" in new Helper() {
      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?days=1")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      val id4: Option[Long] = RecordHelper.insertCallData("4", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), "SIP/fndsjl")

      when(dateHelper.dateByNumberOfDays(1)).thenReturn("2018-06-29 18:00:00")

      val Some(res) = route(app,req)
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2018-06-30 12:00:00",
        "duration" -> "00:05:00",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      )))
    }

    "get the calls since yesterday" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?days=2")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      val id4: Option[Long] = RecordHelper.insertCallData("4", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), "SIP/fndsjl")

      when(dateHelper.dateByNumberOfDays(2)).thenReturn("2018-06-28 18:00:00")

      val Some(res) = route(app,req)
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2018-06-30 12:00:00",
        "duration" -> "00:05:00",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      ),
        Json.obj(
          "start" -> "2018-06-29 17:00:00",
          "duration" -> "00:05:00",
          "src_num" -> "1000",
          "dst_num" -> "2000",
          "status" -> "missed",
          "src_firstname" -> "James",
          "src_lastname" -> "Bond",
          "dst_firstname" -> "Jason",
          "dst_lastname" -> "Bourne"
        )))
    }

    "get the calls of last week" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history?days=7")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("interface" -> "SIP/fndsjl"))

      val srcId: Option[Long] = RecordHelper.insertUser("James", "Bond")
      val dstId: Option[Long] = RecordHelper.insertUser("Jason", "Bourne")

      RecordHelper.insertExtension("1000", "user", srcId.get.toString)
      RecordHelper.insertExtension("2000", "user", dstId.get.toString)

      val id1: Option[Long] = RecordHelper.insertCallData("1", formatter.parseDateTime("2018-05-30 18:00:00").toDate,
        Some(formatter.parseDateTime("2018-05-30 18:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id1.get, formatter.parseDateTime("2018-05-30 18:00:00"), Some(formatter.parseDateTime("2018-05-30 18:05:00")), "SIP/fndsjl")

      val id2: Option[Long] = RecordHelper.insertCallData("2", formatter.parseDateTime("2018-06-23 19:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-23 19:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id2.get, formatter.parseDateTime("2018-06-23 19:00:00"), Some(formatter.parseDateTime("2018-06-23 19:05:00")), "SIP/fndsjl")

      val id3: Option[Long] = RecordHelper.insertCallData("3", formatter.parseDateTime("2018-06-29 17:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-29 17:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id3.get, formatter.parseDateTime("2018-06-29 17:00:00"), Some(formatter.parseDateTime("2018-06-29 17:05:00")), "SIP/fndsjl")

      val id4: Option[Long] = RecordHelper.insertCallData("4", formatter.parseDateTime("2018-06-30 12:00:00").toDate,
        Some(formatter.parseDateTime("2018-06-30 12:05:00").toDate),Some("1000"), Some("2000"))
      RecordHelper.insertCallElement(id4.get, formatter.parseDateTime("2018-06-30 12:00:00"), Some(formatter.parseDateTime("2018-06-30 12:05:00")), "SIP/fndsjl")

      when(dateHelper.dateByNumberOfDays(7)).thenReturn("2018-06-23 18:00:00")

      val Some(res) = route(app,req)
      val json: JsValue = Json.parse(contentAsString(res))

      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2018-06-30 12:00:00",
        "duration" -> "00:05:00",
        "src_num" -> "1000",
        "dst_num" -> "2000",
        "status" -> "missed",
        "src_firstname" -> "James",
        "src_lastname" -> "Bond",
        "dst_firstname" -> "Jason",
        "dst_lastname" -> "Bourne"
      ),
        Json.obj(
          "start" -> "2018-06-29 17:00:00",
          "duration" -> "00:05:00",
          "src_num" -> "1000",
          "dst_num" -> "2000",
          "status" -> "missed",
          "src_firstname" -> "James",
          "src_lastname" -> "Bond",
          "dst_firstname" -> "Jason",
          "dst_lastname" -> "Bourne"
        ),
        Json.obj(
          "start" -> "2018-06-23 19:00:00",
          "duration" -> "00:05:00",
          "src_num" -> "1000",
          "dst_num" -> "2000",
          "status" -> "missed",
          "src_firstname" -> "James",
          "src_lastname" -> "Bond",
          "dst_firstname" -> "Jason",
          "dst_lastname" -> "Bourne"
        )))
    }
  }

  "History Customer controller" should {
    "search call elements by customer in the database with src_num filter" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history/customer")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("filters" -> JsArray(List(Json.obj("field" -> "src_num", "operator" -> "=", "value" -> "123"))), "size" -> 10))

      RecordHelper.insertAgent("James", "Bond","8000",1)
      RecordHelper.insertQueue("test","3000",Some("Test Queue"))

      RecordHelper.insertCallData("1", formatter.parseDateTime("2017-01-01 15:30:20").toDate,
        Some(formatter.parseDateTime("2017-01-01 15:35:20").toDate), Some("123"), Some("3000"))
      RecordHelper.insertCallOnQueue("1","8000","test",QueueCallStatus.Answered)

      RecordHelper.insertCallData("2", formatter.parseDateTime("2017-01-01 16:30:00").toDate,
        Some(formatter.parseDateTime("2017-01-01 16:35:00").toDate),Some( "123"), Some("3000"))
      RecordHelper.insertCallOnQueue("2","8000","test",QueueCallStatus.Answered)

      val Some(res) = route(app,req)

      val json: JsValue = Json.parse(contentAsString(res))
      json shouldEqual Json.obj("total" -> 2, "list" -> JsArray(List(Json.obj(
        "start" -> "2017-01-01 16:30:00",
        "duration" -> "00:05:00",
        "wait_time" -> "00:00:00",
        "agent_name" -> "James Bond",
        "agent_num" -> "8000",
        "queue_name" -> "Test Queue",
        "queue_num" -> "3000",
        "status" -> "answered"
      ),Json.obj(
        "start" -> "2017-01-01 15:30:20",
        "duration" -> "00:05:00",
        "wait_time" -> "00:00:00",
        "agent_name" -> "James Bond",
        "agent_num" -> "8000",
        "queue_name" -> "Test Queue",
        "queue_num" -> "3000",
        "status" -> "answered"
      ))))
    }


    "search call elements by customer in the database with attached_data filter" in new Helper() {
      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history/customer")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("filters" -> JsArray(List(Json.obj("field" -> "key", "operator" -> "=", "value" -> "user"),
          Json.obj("field" -> "value", "operator" -> "=", "value" -> "123"))), "size" -> 10))

      RecordHelper.insertAgent("James", "Bond", "8000", 1)
      RecordHelper.insertQueue("test", "3000", Some("Test Queue"))

      RecordHelper.insertCallData("1", formatter.parseDateTime("2017-01-01 15:30:20").toDate,
        Some(formatter.parseDateTime("2017-01-01 15:35:20").toDate), None, Some("3000"))
      RecordHelper.insertCallOnQueue("1", "8000", "test", QueueCallStatus.Answered)

      val testParser: RowParser[Int] = get[Int]("id").map(id => id)

      val callDataId1: Long = SQL("""SELECT id FROM call_data WHERE uniqueid = '1'""").as(testParser.*).head.toLong
      RecordHelper.insertAttachedData(callDataId1, "user", "123")

      RecordHelper.insertCallData("2", formatter.parseDateTime("2017-01-01 16:30:00").toDate,
        Some(formatter.parseDateTime("2017-01-01 16:35:00").toDate), None, Some("3000"))
      RecordHelper.insertCallOnQueue("2", "8000", "test", QueueCallStatus.Answered)
      val callDataId2: Long = SQL("""SELECT id FROM call_data WHERE uniqueid = '2'""").as(testParser.*).head.toLong
      RecordHelper.insertAttachedData(callDataId2, "user", "123")

      val Some(res) = route(app,req)
      val json: JsValue = Json.parse(contentAsString(res))
      json shouldEqual Json.obj("total" -> 2, "list" -> JsArray(List(Json.obj(
        "start" -> "2017-01-01 16:30:00",
        "duration" -> "00:05:00",
        "wait_time" -> "00:00:00",
        "agent_name" -> "James Bond",
        "agent_num" -> "8000",
        "queue_name" -> "Test Queue",
        "queue_num" -> "3000",
        "status" -> "answered"
      ),Json.obj(
        "start" -> "2017-01-01 15:30:20",
        "duration" -> "00:05:00",
        "wait_time" -> "00:00:00",
        "agent_name" -> "James Bond",
        "agent_num" -> "8000",
        "queue_name" -> "Test Queue",
        "queue_num" -> "3000",
        "status" -> "answered"
      ))))
    }
  }

"History Agent controller" should {
    "find call elements by agent for the last 7 days in the database" in new Helper() {

      val req: FakeRequest[AnyContentAsJson] = FakeRequest("POST", s"$context/history/agent")
        .withSession(("username", "test"))
        .withJsonBody(Json.obj("agentNum" -> "7200"))

      val firstUser: Option[Long] = RecordHelper.insertUser("John", "Doe")
      val secondUser: Option[Long] = RecordHelper.insertUser("Mark", "Us")

      RecordHelper.insertExtension("1000", "user", firstUser.get.toString)
      RecordHelper.insertExtension("2000", "user", secondUser.get.toString)

      val idToFound: Option[Long] = RecordHelper.insertCallData("123", formatter.parseDateTime("2021-01-19 15:30:21").toDate,
        Some(formatter.parseDateTime("2021-01-19 15:36:22").toDate), Some("1000"), Some("3000"), CallDirection.Incoming, Some("7200"))
      RecordHelper.insertCallElement(idToFound.get, formatter.parseDateTime("2021-01-19 15:30:21"), Some(formatter.parseDateTime("2021-01-19 15:36:22")), "SIP/fndsjl")

      val idTooOld: Option[Long] = RecordHelper.insertCallData("231", formatter.parseDateTime("2021-01-10 15:30:21").toDate,
        Some(formatter.parseDateTime("2013-01-01 15:36:22").toDate), Some("1000"), Some("3000"), CallDirection.Incoming, Some("7200"))
      RecordHelper.insertCallElement(idTooOld.get, formatter.parseDateTime("2021-01-10 15:30:21"), Some(formatter.parseDateTime("2021-01-10 15:36:22")), "SIP/fndsjl")

      val idToDiscard: Option[Long] = RecordHelper.insertCallData("456", formatter.parseDateTime("2021-01-19 16:30:21").toDate,
        Some(formatter.parseDateTime("2021-01-19 16:36:22").toDate), Some("2000"), Some("1000"))
      RecordHelper.insertCallElement(idToDiscard.get, formatter.parseDateTime("2021-01-19 16:30:21"), Some(formatter.parseDateTime("2021-01-19 16:36:22")), "SIP/sapanj")

      when(dateHelper.dateByNumberOfDays(7)).thenReturn("2021-01-12 15:00:00")
      val Some(res) = route(app, req)

      val json: JsValue = contentAsJson(res)
      json shouldEqual JsArray(List(Json.obj(
        "start" -> "2021-01-19 15:30:21",
        "duration" -> "00:06:01",
        "src_num" -> "1000",
        "dst_num" -> "3000",
        "status" -> "abandoned",
        "src_firstname" -> "John",
        "src_lastname" -> "Doe",
        "dst_firstname" -> JsNull,
        "dst_lastname" -> JsNull
      )))
    }
  }
}
