DROP TABLE IF EXISTS "cel" CASCADE;
CREATE TABLE "cel" (
 "id" serial ,
 "eventtype" VARCHAR (30) NOT NULL ,
 "eventtime" timestamp NOT NULL ,
 "userdeftype" VARCHAR(255) NOT NULL ,
 "cid_name" VARCHAR (80) NOT NULL ,
 "cid_num" VARCHAR (80) NOT NULL ,
 "cid_ani" VARCHAR (80) NOT NULL ,
 "cid_rdnis" VARCHAR (80) NOT NULL ,
 "cid_dnid" VARCHAR (80) NOT NULL ,
 "exten" VARCHAR (80) NOT NULL ,
 "context" VARCHAR (80) NOT NULL ,
 "channame" VARCHAR (80) NOT NULL ,
 "appname" VARCHAR (80) NOT NULL ,
 "appdata" VARCHAR (512) NOT NULL ,
 "amaflags" int NOT NULL ,
 "accountcode" VARCHAR (20) NOT NULL ,
 "peeraccount" VARCHAR (20) NOT NULL ,
 "uniqueid" VARCHAR (150) NOT NULL ,
 "linkedid" VARCHAR (150) NOT NULL ,
 "userfield" VARCHAR (255) NOT NULL ,
 "peer" VARCHAR (80) NOT NULL ,
 "call_log_id" INTEGER DEFAULT NULL,
 "extra" TEXT,
 PRIMARY KEY("id")
);

CREATE INDEX "cel__idx__uniqueid" ON "cel"("uniqueid");
CREATE INDEX "cel__idx__eventtime" ON "cel"("eventtime");
CREATE INDEX "cel__idx__call_log_id" ON "cel" ("call_log_id");

