DROP TABLE IF EXISTS preferred_callback_period CASCADE;

CREATE TABLE preferred_callback_period (
    uuid UUID PRIMARY KEY,
    name VARCHAR(128) NOT NULL DEFAULT '',
    period_start TIME WITHOUT TIME ZONE NOT NULL,
    period_end TIME WITHOUT TIME ZONE NOT NULL,
    "default" BOOLEAN DEFAULT FALSE
);
CREATE TRIGGER preferred_callback_period_uuid_trigger BEFORE INSERT ON preferred_callback_period FOR EACH ROW EXECUTE PROCEDURE uuid_generator();