DROP TABLE IF EXISTS "transfers" CASCADE;
CREATE TABLE transfers (
    id SERIAL PRIMARY KEY,
    callidfrom VARCHAR(32),
    callidto VARCHAR(32)
);
CREATE INDEX transfers__idx__callidfrom ON transfers(callidfrom);
