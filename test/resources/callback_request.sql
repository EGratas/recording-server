DROP TABLE IF EXISTS callback_request CASCADE;

CREATE TABLE callback_request (
    uuid UUID PRIMARY KEY,
    list_uuid UUID NOT NULL,
    phone_number VARCHAR(40),
    mobile_phone_number VARCHAR(40),
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    company VARCHAR(128),
    description TEXT,
    agent_id INTEGER,
    clotured BOOLEAN DEFAULT FALSE,
    preferred_callback_period_uuid UUID,
    due_date DATE NOT NULL DEFAULT now(),
    reference_number SERIAL NOT NULL
);
CREATE TRIGGER callback_request_uuid_trigger BEFORE INSERT ON callback_request FOR EACH ROW EXECUTE PROCEDURE uuid_generator();