DROP TABLE IF EXISTS queuefeatures CASCADE;

CREATE TABLE queuefeatures (
    id SERIAL PRIMARY KEY,
    name VARCHAR(128),
    number VARCHAR(40),
    displayname varchar(128)
);
