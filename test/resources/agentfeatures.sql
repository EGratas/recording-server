DROP TABLE IF EXISTS agentfeatures CASCADE;

CREATE TABLE agentfeatures (
    id SERIAL PRIMARY KEY,
    numgroup INTEGER NOT NULL,
    firstname VARCHAR(128),
    lastname VARCHAR(128),
    number VARCHAR(40)
);