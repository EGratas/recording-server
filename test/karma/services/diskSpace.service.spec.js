'use strict';

describe("Disk space service", function() {

  var diskSpace, http;

  beforeEach(angular.mock.module('Disk'));

  beforeEach(inject(function ($httpBackend) {
    http = $httpBackend;
  }));

  beforeEach(angular.mock.inject(function(_diskSpace_) {
    diskSpace = _diskSpace_;
  }));

  it("instantiate the service", function () {
    expect(diskSpace).not.toBeUndefined;
  });

  it("gets disk space from server", inject(function($rootScope) {
    http.when('GET','partitions').respond(200, {data: 'data'} );
    http.expectGET('partitions');
    var promise = diskSpace.getJson();
    http.flush();
    $rootScope.$apply();
    promise.then(function(res) {
      expect(res.data).toBe('data');
    }, function(error) {
      assert.fail(error);
    });
  }));

  it("calculates chart data", inject(function($rootScope) {
    http.when('GET','partitions').respond(200, [{"name":"Partition des enregistrements",
        "mountPoint":"/var/log",
        "freeSpace":107374182400,
        "totalSpace":1073741824000}]
    );
    http.expectGET('partitions');
    var promise = diskSpace.getChartData();
    http.flush();
    $rootScope.$apply()
    promise.then(function(res) {
      expect(res).toEqual(
        [{"name":"Partition des enregistrements",
        "mountPoint":"/var/log",
        "freeSpace":107374182400,
        "totalSpace":1073741824000,
        "chartData":{"series":["GB"],
        "data":[{"x":"Free",
        "y":["100.00"],
        "tooltip":"100.00 GB"},
        {"x":"Used",
        "y":["900.00"],
        "tooltip":"900.00 GB"}]},
        "chartConfig":{"title":"Partition des enregistrements (/var/log)",
        "colors":["blue",
        "red"],
        "legend":{"display":true,
        "position":"right"}},
        "usedSpacePercent":90}]
      );
    }, function(error) {
      assert.fail(error);
    });
  }));

  it("does not get warning message when disks are not full", inject(function($rootScope) {
    http.when('GET','partitions').respond(200, [{"name":"Partition des enregistrements",
        "mountPoint":"/var/log",
        "freeSpace":90,
        "totalSpace":100}]
    );
    http.expectGET('partitions');
    var promise = diskSpace.getAlertMessage();
    http.flush();
    $rootScope.$apply();
    promise.then(function(res) {
      expect(res).toEqual('');
    }, function(error) {
      assert.fail(error);
    });
  }));

  it("get warning message when disks are almost full", inject(function($rootScope) {
    http.when('GET','partitions').respond(200, [{"name":"Partition des enregistrements",
        "mountPoint":"/var/log",
        "freeSpace":10,
        "totalSpace":100},
        {"name":"Disk II",
        "mountPoint":"/var/spool/recordings",
        "freeSpace":5.55661215,
        "totalSpace":100}
        ]
    );
    http.expectGET('partitions');
    var promise = diskSpace.getAlertMessage();
    http.flush();
    $rootScope.$apply();
    promise.then(function(res) {
      expect(res).toEqual('ATTENTION\n\n' +
        'Les disques sont bientôt pleins!\n' +
        '\n' +
        'Disque Partition des enregistrements\n(/var/log):\n90.00 % utilisés.\n\n' +
    	'Disque Disk II\n(/var/spool/recordings):\n94.44 % utilisés.'
      );
    }, function(error) {
      assert.fail(error);
    });
  }));

});