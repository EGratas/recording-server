describe('listenPlayEvent directive', function() {
  var scope;
  var element;
  var mockEvent;
  var $http;

  beforeEach(angular.mock.module('recording-server'));

  beforeEach(angular.mock.inject(function($rootScope, $compile, _$http_) {
    scope = $rootScope.$new();
    $http = _$http_;
    element = $compile('<audio listen-play-event></audio>')(scope);
    $rootScope.$digest();

    mockEvent= {
      type: 'play'
    };
  }));

  it('calls recording API to log play event from audio controls', function(){
    spyOn($http, 'get');
    scope.record = {id: 1};
    element.triggerHandler(mockEvent);
    expect($http.get).toHaveBeenCalledWith('records/'+scope.record.id+'/audio/listen');
  });

});