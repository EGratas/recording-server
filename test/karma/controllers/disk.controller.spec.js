'use strict';

describe("Disk controller", function() {

  var diskSpace, ctrl, rootScope, scope;

  beforeEach(angular.mock.module('Disk'));

  beforeEach(angular.mock.inject(function($rootScope, $controller, _diskSpace_) {
    rootScope =$rootScope;
    scope = $rootScope.$new();
    diskSpace = _diskSpace_;
    ctrl = $controller('DiskController', {
        '$scope': scope,
        'diskSpace': diskSpace});
  }));

  it("instantiate the controller", function () {
    expect(ctrl).not.toBeUndefined;
  });

});