describe('Number controller', function() {
  var scope;
  var ctrl;

  beforeEach(angular.mock.module('recording-exclusion'));

  beforeEach(angular.mock.inject(function($rootScope, $controller) {
    scope = $rootScope.$new();
    ctrl = $controller('NumberController', { '$scope': scope});
  }));

  it('can instantiate controller', function(){
    expect(ctrl).not.toBeUndefined();
  });

});
