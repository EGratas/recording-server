package models.authentication

import models.{SqlUtils, XivoObjects}
import org.joda.time.DateTime
import org.joda.time.format.{DateTimeFormat, DateTimeFormatter}
import play.api.libs.json.{JsArray, JsValue}
import java.sql.Connection

abstract class Right {
  def buildWhereClause()(implicit c: Connection): String
}

abstract class LimitedRight(
    queueIds: List[Int],
    groupIds: List[Int],
    incallIds: List[Int]
) extends Right {
  override def buildWhereClause()(implicit c: Connection): String = {
    var res       = ""
    val agentNums = XivoObjects.agentNumbersForGroups(groupIds)
    res += s""" ${SqlUtils.createInClauseOrFalse("cq.agent_num", agentNums)}
    OR ${SqlUtils.createInClauseOrFalse("cd.src_agent", agentNums)}
    OR ${SqlUtils.createInClauseOrFalse("cd.dst_agent", agentNums)} """

    val queueNames = queueIds.flatMap(XivoObjects.queueNameById(_))
    res += s" OR ${SqlUtils.createInClauseOrFalse("cq.queue_ref", queueNames)} "

    val incallExtens = incallIds.flatMap(XivoObjects.incallExtenById(_))
    res += s" OR ${SqlUtils.createInClauseOrFalse("cd.dst_num", incallExtens)} "
    res
  }
}

case class AdminRight() extends Right {
  override def buildWhereClause()(implicit c: Connection): String = "TRUE"
}
case class SupervisorRight(
    queueIds: List[Int],
    groupIds: List[Int],
    incallIds: List[Int],
    recordingAccess: Boolean = true,
    dissuasionAccess: Boolean = false
) extends LimitedRight(queueIds, groupIds, incallIds)
case class TeacherRight(
    queueIds: List[Int],
    groupIds: List[Int],
    incallIds: List[Int],
    start: DateTime,
    end: DateTime
) extends LimitedRight(queueIds, groupIds, incallIds)

object Right {

  def fromJson(value: JsValue): Right =
    (value \ "type").as[String] match {
      case "admin" => AdminRight()
      case "supervisor" =>
        SupervisorRight(
          (value \ "data" \ "queueIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "groupIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "incallIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "recordingAccess").asOpt[Boolean].getOrElse(true),
          (value \ "data" \ "dissuasionAccess").asOpt[Boolean].getOrElse(false)
        )
      case "teacher" =>
        TeacherRight(
          (value \ "data" \ "queueIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "groupIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "incallIds")
            .asOpt[JsArray]
            .getOrElse(JsArray())
            .value
            .map(v => v.as[Int])
            .toList,
          (value \ "data" \ "start").as[String],
          (value \ "data" \ "end").as[String]
        )
    }
}

object TeacherRight {

  val formatter: DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd")

  def apply(
      queueIds: List[Int],
      groupIds: List[Int],
      incallIds: List[Int],
      start: String,
      end: String
  ): TeacherRight =
    TeacherRight(
      queueIds,
      groupIds,
      incallIds,
      formatter.parseDateTime(start).withTime(0, 0, 0, 0),
      formatter.parseDateTime(end).withTime(23, 59, 59, 0)
    )
}
