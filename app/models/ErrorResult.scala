package models

import play.api.Logger
import play.api.libs.json.{JsString, JsValue, Json, Writes}
import play.api.mvc.{Result, Results}

trait ErrorResult {
  val log = Logger("ErrorResult")
  def toResult: Result
}

trait ErrorType { def error: String }
case object NotHandledError extends ErrorType { val error = "NotHandledError" }

object ErrorType {
  implicit val writes = new Writes[ErrorType] {
    def writes(e: ErrorType): JsValue = JsString(e.error)
  }
}

object GenericError {
  implicit val genericErrorWrites: Writes[GenericError] =
    Json.writes[GenericError]
}

case class GenericError(code: ErrorType, message: String) extends ErrorResult {
  override def toResult: Result = {
    log.error(this.message)
    Results.InternalServerError(Json.toJson(this))
  }
}
