package models

import java.sql.Connection
import java.util.Date

import anorm.SqlParser.{get, _}
import anorm.{SQL, _}
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.postgresql.util.PSQLException
import play.api.Logger

import scala.util.{Failure, Success, Try}

object Calls {

  val logger = Logger(getClass.getName)

  def callExistsAfterTime(time: Date)(implicit c: Connection): Boolean =
    (SQL("SELECT start_time FROM call_data WHERE start_time >= {time}").on(
      Symbol("time") -> time
    ) as scalar[DateTime].singleOpt).isDefined

  def addAttachedDataById(callDataId: Long, dataList: List[AttachedData])(
      implicit c: Connection
  ) =
    dataList.foreach(data =>
      SQL(
        "INSERT INTO attached_data(id_call_data, key, value) VALUES ({cdId}, {key}, {value})"
      )
        .on(
          Symbol("cdId")  -> callDataId,
          Symbol("key")   -> data.key,
          Symbol("value") -> data.value
        )
        .executeUpdate()
    )

  def addAttachedDataByCallId(
      callDataUniqueId: String,
      callReason: AttachedData
  )(implicit c: Connection): AttachedDataReponse = {
    try {
      if (checkDatetimeFormat(callReason.value)) {
        val callDataId: Option[Int] =
          SQL(
            s"""SELECT id FROM call_data WHERE call_data.uniqueid = {linkedid}"""
          )
            .on(Symbol("linkedid") -> callDataUniqueId)
            .as((get[Int]("id")).*)
            .headOption
        callDataId match {
          case Some(cdId) =>
            SQL(
              "INSERT INTO attached_data(id_call_data, key, value) VALUES ({cdId}, {key}, {value})"
            )
              .on(
                Symbol("cdId")  -> cdId,
                Symbol("key")   -> callReason.key,
                Symbol("value") -> callReason.value
              )
              .executeInsert()
            SuccessAttachedDataReponse(
              s"Successfully inserted attached data with call id: $callDataId"
            )
          case None =>
            FailedAttachedDataReponse(
              s"Unable to find call in call_data by callid: $callDataUniqueId"
            )
        }
      } else {
        FailedAttachedDataReponse(
          s"Call purge date is not correctly formatted: $callReason"
        )
      }
    } catch {
      case e: PSQLException =>
        logger.error(e.getMessage)
        FailedAttachedDataReponse(e.getMessage)
    }
  }

  def checkDatetimeFormat(callPurgeDate: String): Boolean = {
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    Try[DateTime](formatter.parseDateTime(callPurgeDate)) match {
      case Success(isDate) =>
        true
      case Failure(notDate) =>
        false
    }
  }
}

abstract class AttachedDataReponse()
case class SuccessAttachedDataReponse(result: String)
    extends AttachedDataReponse()
case class FailedAttachedDataReponse(result: String)
    extends AttachedDataReponse()
