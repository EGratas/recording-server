package models

import play.api.libs.functional.syntax._
import play.api.libs.json.{JsPath, Reads}

case class CallbackTicketPatch(
    status: Option[CallbackStatus.CallbackStatus],
    comment: Option[String],
    callid: Option[String]
)

object CallbackTicketPatch {
  implicit val reads: Reads[CallbackTicketPatch] = (
    (JsPath \ "status")
      .readNullable[String]
      .map(_.map(CallbackStatus.withName)) and
      (JsPath \ "comment").readNullable[String] and
      (JsPath \ "callid").readNullable[String]
  )(CallbackTicketPatch.apply _)
}
