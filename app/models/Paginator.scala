package models

case class Paginator(offset: Int, size: Int)
