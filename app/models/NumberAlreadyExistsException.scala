package models

class NumberAlreadyExistsException(number: String)
    extends Exception(s"$number already exists")
