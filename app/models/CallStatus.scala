package models

object CallStatus extends Enumeration {
  type CallStatus = Value
  val Emitted        = Value("emitted")
  val Answered       = Value("answered")
  val Missed         = Value("missed")
  val Ongoing        = Value("ongoing")
  val Abandoned      = Value("abandoned")
  val DivertCaRatio  = Value("divert_ca_ratio")
  val DivertWaitTime = Value("divert_waittime")
  val Closed         = Value("closed")
  val Full           = Value("full")
  val JoinEmpty      = Value("joinempty")
  val LeaveEmpty     = Value("leaveempty")
  val Timeout        = Value("timeout")
  val ExitWithKey    = Value("exit_with_key")
}
