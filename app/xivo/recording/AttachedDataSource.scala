package xivo.recording

import akka.NotUsed
import akka.stream.Materializer
import akka.stream.scaladsl.{Concat, Source}
import models.{AttachedData, Record}
import play.api.db.{Database, NamedDatabase}

import java.sql.Timestamp
import javax.inject.Inject
import scala.concurrent.ExecutionContext.Implicits.global
import scala.util.{Failure, Success}

class AttachedDataSource @Inject() (@NamedDatabase("stats") dbStats: Database)(
    implicit val materializer: Materializer
) extends DirVisitorStream {

  private def getAttachedDataToPurgeSource(
      fetchSize: Int
  ): Source[String, Unit] = {
    val connection = dbStats.getConnection(autocommit = false)
    logger.info("[1] Retrieving attached data with key to purge")
    Record
      .getAttachedDataToPurge(
        AttachedData.PurgeKey,
        AttachedData.RecordingKey,
        fetchSize
      )(connection, materializer)
      .log(name = "getAttachedDataToPurge")
      .addAttributes(logLevels)
      .mapMaterializedValue(_.onComplete {
        case Success(s) =>
          logger.info(
            s"[1] Retrieved attached data with key to purge (retrieved: $s)"
          )
          connection.close()
        case Failure(f) =>
          println(
            s"[1] Error while retrieving attached data with key to purge. $f"
          )
          connection.close()
      })
  }

  private def getAttachedDataToPurgeWithoutKeySource(
      fetchSize: Int,
      refTime: Timestamp
  ): Source[String, Unit] = {
    val connection = dbStats.getConnection(autocommit = false)
    logger.info("[2] Retrieving attached data without key to purge")
    Record
      .getAttachedDataToPurgeWithoutKey(
        AttachedData.PurgeKey,
        AttachedData.RecordingKey,
        refTime,
        fetchSize
      )(connection, materializer)
      .log(name = "getAttachedDataToPurgeWithoutKey")
      .addAttributes(logLevels)
      .mapMaterializedValue(_.onComplete {
        case Success(s) =>
          logger.info(
            s"[2] Retrieved attached data without key to purge (retrieved: $s)"
          )
          connection.close()
        case Failure(f) =>
          println(
            s"[2] Error while retrieving attached data without key to purge. $f"
          )
          connection.close()
      })
  }

  def getAttachedDataToPurge(
      refTime: Timestamp,
      fetchSize: Int = 1500
  ): Source[String, NotUsed] = {
    Source.combine(
      getAttachedDataToPurgeSource(fetchSize),
      getAttachedDataToPurgeWithoutKeySource(fetchSize, refTime)
    )(Concat(_))
  }
}
