import com.google.inject.AbstractModule
import org.slf4j.LoggerFactory
import startup.ApplicationStart

class Startup extends AbstractModule {

  val logger = LoggerFactory.getLogger(getClass)

  override def configure() = {
    logger.info("Configuring Startup Module")
    bind(classOf[ApplicationStart]).asEagerSingleton()
  }
}
