angular.module('recording-exclusion',[])
.controller('SdaController', function($scope, $http) {
    function getSdas() {
      $http.get('filtered_incalls').then(function(json) {
        $scope.sdas = json.data;
        $scope.error = null;
      }, function(error) {
        $log.error('Unable to get sda filtered list', error);
      });
    }

    function displayError(response) {
      $scope.error = response;
    }

    $scope.deleteSda = function(sda) {
      $http.delete('filtered_incalls/' + sda).then(getSdas, function(error) {
        $log.error('Unable to delete sda number '+sda, error);
      });
    }

    $scope.createSda = function() {
      $http.post('filtered_incalls', JSON.stringify($scope.newSda)).then(getSdas, displayError);
    }

    getSdas();
})
.controller('NumberController', function($scope, $http) {
    function getNumbers() {
      $http.get('filtered_internal_numbers').then(function(json) {
        $scope.numbers = json.data;
        $scope.error = null;
      }, function(error) {
        $log.error('Unable to get internal number filtered list', error);
      });
    }

    function displayError(response) {
      $scope.error = response;
    }

    $scope.deleteNumber = function(number) {

      $http.delete('filtered_internal_numbers/' + number).then(getNumbers, function(error) {
        $log.error('Unable to delete internal number '+number, error);
      });
    }

    $scope.createNumber = function() {
      $http.post('filtered_internal_numbers', JSON.stringify($scope.newNumber)).then(getNumbers, displayError);
    }

    getNumbers();
});
