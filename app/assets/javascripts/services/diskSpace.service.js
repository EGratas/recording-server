angular.module('Disk').factory('diskSpace', function($http, $log) {
  var language = document.getElementsByTagName("body")[0].getAttribute("lang");
  var freeLabel= (language=="fr")? "Libre" : "Free" ;
  var usedLabel= (language=="fr")? "Utilisé" : "Used" ;
  var getJson = function() {
    return new Promise(function(resolve, reject) {
      $http.get('partitions').then(function(response) {
        resolve(response.data);
      }, function(error) {
        $log.error('Unable to get disk space because of ', error);
        reject();
      });
    });
  };

  var getChartData = function() {
    return new Promise(function(resolve, reject) {
      getJson().then(function(json) {
        resolve(processJson(json));
      });
    });
  };

  var getAlertMessage = function() {
    return new Promise(function(resolve, reject) {
      getJson().then(function(json) {
        data = processJson(json);
        maxUsed = 80;
        msg = "";
        for(var i = 0; i < data.length; i++) {
          d = data[i];
          if (d.usedSpacePercent > maxUsed) {
            if (msg == '')
                msg = "ATTENTION\n\nLes disques sont bientôt pleins!";
            msg += "\n\nDisque " +  d.name +
                "\n(" + d.mountPoint + "):\n" +
                d.usedSpacePercent.toPrecision(4) + " % utilisés.";
          }
        }
        resolve(msg);
      });
    });
  };

  var processJson = function(json) {
    var partitions = json;
    var toGb = 1073741824;
    for(var i = 0; i < json.length; i++) {
      var p = partitions[i];
      p.chartData = {
        series: ['GB'],
        data: [{
          x: freeLabel,
          y: [(p.freeSpace / toGb).toFixed(2)],
          tooltip: (p.freeSpace / toGb).toFixed(2) + ' GB'
        }, {
          x: usedLabel,
          y: [((p.totalSpace - p.freeSpace) / toGb).toFixed(2)],
          tooltip: ((p.totalSpace - p.freeSpace) / toGb).toFixed(2) + ' GB'
        }]
      };
      p.chartConfig = {
        title : p.name + ' (' + p.mountPoint + ')',
        colors: ['blue', 'red'],
        legend: {
          display: true,
          position: 'right'
        }
      };
      p.usedSpacePercent = (p.totalSpace - p.freeSpace) / p.totalSpace * 100;
    }
    return partitions;
  }

  return {
    getChartData: getChartData,
    getAlertMessage: getAlertMessage,
    getJson: getJson
  };
})
