var csrfToken = getCookie("XSRF-TOKEN");
var authToken = $('#rs_auth_token').val();

function logIn() {
    var username = $('#rs_username').val();
    var password = $('#rs_password').val();

    localStorage.setItem('sampleUsername', username);
    localStorage.setItem('samplePassword', password);

    console.log("sign in " + " : " + username + " " + password);

    makeAuthCall("POST", "/recording/login?isSample=true",
        {"login": username, "mdp": password}).success(function(user) {
        console.log("Logging in...");
        $('#rs_csrf_token').val(user.token);

        $('#rs_logon_panel').hide();
        $('#nav-tabs').show();
        $('#tab-content').show();
        $('#rs_restart').show();
        $('#welcome').hide();
    }).fail(function() {
        var error = "Impossible to retrieve auth token for user " + username;
        generateMessage(error, true, true);
    });
}

function logOut() {
    makeAuthCall("GET", "/recording/logout?isSample=true").success(function() {
        console.log("Logging out...");
        cleanAll();

        $('#rs_logon_panel').show();
        $('#nav-tabs').hide();
        $('#tab-content').hide();
        $('#rs_restart').hide();
        $('#welcome').show();

        $('#show_search_filters').show();
        $('#hide_search_filters').hide();
    }).fail(function() {
        var error = "Log out failed!";
        generateMessage(error, true, true);
    });
}

function cleanAll() {
    //Search
    $('#rs_search_agent').val('');
    $('#rs_search_queue').val('');
    $('#rs_search_start').val('');
    $('#rs_search_end').val('');
    $('#rs_search_callee').val('');
    $('#rs_search_caller').val('');
    $('#rs_search_direction').val('');
    $('#rs_search_queueCallStatus').val('');
    $('#rs_search_key').val('');
    $('#rs_search_value').val('');
    $('#rs_search_page').val('');
    $('#rs_search_pageSize').val('');
    cleanSearch();

    //Search by Call ID
    $('#rs_sbci_callid').val('');
    cleanSearchByCallID();

    //Retrieve Audio File
    $('#rs_raf_file-id').val('');
    $('#rs_raf_action').val('');
    cleanRetrieveAudioFile();

    //Attach Call Data
    $('#rs_acd_call-data-id').val('');
    $('#rs_acd_key').val('');
    $('#rs_acd_value').val('');
    cleanAttachCallData();

    //Agent Call History
    $('#rs_ach_size').val('');
    $('#rs_ach_agent_num').val('');
    cleanAgentCallHistory();

    //User Call History
    $('#rs_uch_size').val('');
    $('#rs_uch_days').val('');
    $('#rs_uch_interface').val('');
    cleanUserCallHistory();

    //Customer Call History
    $('#rs_cch_size').val('');
    $('#rs_cch_field').val('');
    $('#rs_cch_operator').val('');
    $('#rs_cch_value').val('');
    cleanCustomerCallHistory();

    //Last Agent for Number
    $('#rs_lafn_callerNo').val('');
    $('#rs_lafn_since').val('');
    cleanLastAgentForNumber();
}

function cleanSearch() {
    $('#search').empty();
}

function cleanSearchByCallID() {
    $('#search_by_call_id').empty();
}

function cleanRetrieveAudioFile() {
    $('#retrieve_audio_file').empty();
}

function cleanAttachCallData() {
    $('#attach_call_data').empty();
}

function cleanAgentCallHistory() {
    $('#agent_call_history').empty();
}

function cleanUserCallHistory() {
    $('#user_call_history').empty();
}

function cleanCustomerCallHistory() {
    $('#customer_call_history').empty();
}

function cleanLastAgentForNumber() {
    $('#last_agent_for_number').empty();
}

function printSearch(calls) {
    console.log('Searching recorded calls...' + JSON.stringify(calls));
    $('#search').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(calls) + '</code></pre></li>');
}

function printSearchByCallId(call) {
    console.log('Searching call by id...' + JSON.stringify(call));
    $('#search_by_call_id').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(call) + '</code></pre></li>');
}

function printRetrieveAudioFile(file, fileid, action) {
    console.log('Retrieving audio file...' + JSON.stringify(file));
    if(action === "result") {
        $('#retrieve_audio_file').prepend(
            '<audio controls controlsList="nodownload" oncontextmenu="return false;" listen-play-event>' +
            '<source src="records/' + fileid + '/audio/result">' +
            'Your browser does not support the element <code>audio</code>.' +
            '</audio>'
        );
    }
    else if(action === "listen") {
        $('#retrieve_audio_file').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(file) + '</code></pre></li>');
    }
    else if(action === "download") {
        $('#retrieve_audio_file').prepend(
            '<p>Download the file:<a href="records/' + fileid + '/audio/download" target="_blank" download title="Download file" class="download">\n' +
            '<i class="glyphicon glyphicon-save"></i>\n' +
            '</a> </p>'
        );
    }
}

function printAttachCallData() {
    console.log('Attaching call data...');
    $('#attach_call_data').prepend('<li class="list-group-item"><pre><code> Call data was added to the table attached_data  </code></pre></li>');
}

function printAgentCallHistory(callHistory) {
    console.log('Getting agent call history... ' + JSON.stringify(callHistory));
    $('#agent_call_history').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callHistory) + '</code></pre></li>');
}

function printUserCallHistory(callHistory) {
    console.log('Getting user call history... ' + JSON.stringify(callHistory));
    $('#user_call_history').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callHistory) + '</code></pre></li>');
}

function printCustomerCallHistory(callHistory) {
    console.log('Getting customer call history... ' + JSON.stringify(callHistory));
    $('#customer_call_history').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(callHistory) + '</code></pre></li>');
}

function printLastAgentForNumber(agent) {
    console.log('Getting last agent for number...' + JSON.stringify(agent));
    $('#last_agent_for_number').prepend('<li class="list-group-item"><pre><code>' + JSON.stringify(agent) + '</code></pre></li>');
}

function generateMessage(message, isError) {
    var className = (isError) ? 'danger' : 'success';
    $('#main_errors').html('<p class="alert alert-'+className+' alert-dismissible"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>' + new Date().toLocaleString() + ": " + JSON.stringify(message) + '</p>');
    console.log(message);
}

$(function() {
    console.log("Initializing sample page...");

    logOut();

    var recording_server = window.location.hostname+":"+window.location.port;
    $('#recording_server').val(recording_server);

    $('#rs_username').val(localStorage.getItem('sampleUsername'));
    $('#rs_password').val(localStorage.getItem('samplePassword'));

    $('#rs_sign_in').click(function() {
        logIn();
    });

    $('#rs_restart').click(function() {
        logOut();
    });

    $('#show_search_filters').click(function() {
        $('#show_search_filters').hide();
        $('#hide_search_filters').show();
    });

    $('#hide_search_filters').click(function() {
        $('#show_search_filters').show();
        $('#hide_search_filters').hide();
    });

    $('#rs_clean_search').click(function() {
        cleanSearch();
    });

    $('#rs_clean_search_by_call_id').click(function() {
        cleanSearchByCallID();
    });

    $('#rs_clean_retrieve_audio_file').click(function() {
        cleanRetrieveAudioFile();
    });

    $('#rs_clean_attach_call_data').click(function() {
        cleanAttachCallData();
    });

    $('#rs_clean_agent_call_history').click(function() {
        cleanAgentCallHistory();
    });

    $('#rs_clean_user_call_history').click(function() {
        cleanUserCallHistory();
    });

    $('#rs_clean_customer_call_history').click(function() {
        cleanCustomerCallHistory();
    });

    $('#rs_clean_last_agent_for_number').click(function() {
       cleanLastAgentForNumber();
    });

    $('#rs_get_search').click(function() {
        var agent = $('#rs_search_agent').val();
        var queue = $('#rs_search_queue').val();
        var start = $('#rs_search_start').val();
        var end = $('#rs_search_end').val();
        var callee = $('#rs_search_callee').val();
        var caller = $('#rs_search_caller').val();
        var direction = $('#rs_search_direction').val();
        var queueCallStatus = $('#rs_search_queueCallStatus').val();
        var key = $('#rs_search_key').val();
        var value = $('#rs_search_value').val();

        if(!key && value) generateMessage("Cannot search recorded calls with a value and no key", true, true);
        else {
            var page = $('#rs_search_page').val();
            var pageSize = $('#rs_search_pageSize').val();

            var json = {};

            if(agent) json.agent = agent;
            if(queue) json.queue = queue;
            if(start) json.start = start + " 00:00:00";
            if(end) json.end = end + " 23:59:59";
            if(callee) json.callee = callee;
            if(caller) json.caller = caller;
            if(direction) json.direction = direction;
            if(queueCallStatus) json.queueCallStatus = queueCallStatus;
            if(key) json.key = key;
            if(value) json.value = value;

            var param = "";
            if(page && pageSize) param = "?page=" + page + "&pageSize=" + pageSize;
            else if(page) param = "?page=" + page;
            else if(pageSize) param = "?pageSize=" + pageSize;

            makeAuthCall('POST', "/recording/records/search" + param, json).success(function(calls) {
                printSearch(calls);
            }).fail(function() {
                var error = "Impossible to search recorded calls";
                generateMessage(error, true, true);
            });
        }
    });

    $('#rs_get_search_by_call_id').click(function() {
       var callid = $('#rs_sbci_callid').val();

       if(!callid) generateMessage("Cannot search a recorded call by its ID without an ID", true, true);
       else {
           var param = "?callid=" + callid;

           makeAuthCall('POST', "/recording/records/callid_search" + param).success(function(call) {
               printSearchByCallId(call);
           }).fail(function() {
              var error = "Impossible to search calls by call ID";
              generateMessage(error, true, true);
           });
       }
    });

    $('#rs_get_retrieve_audio_file').click(function() {
       var fileid = $('#rs_raf_file-id').val();
       var action = $('#rs_raf_action').val();
       if(!fileid) generateMessage("Cannot retrieve an audio file without its ID", true, true);
       else if(!action) generateMessage("Cannot retrieve an audio file if the action is not specified", true, true);
       else if(action !== "result" && action !== "listen" && action !== "download") generateMessage("To retrieve an audio file, the action should be either 'result', 'listen' or 'download'", true, true);
       else {
           makeAuthCall('GET', "/recording/records/" + fileid + "/audio/" + action).success(function(file) {
               printRetrieveAudioFile(file, fileid, action);
           }).fail(function() {
               var error = "Impossible to retrieve audio file";
               generateMessage(error, true, true);
           });
       }
    });

    $('#rs_get_attach_call_data').click(function() {
       var calldataid = $('#rs_acd_call-data-id').val();
       var key = $('#rs_acd_key').val();
       var value = $('#rs_acd_value').val();

       if(!calldataid) generateMessage("Cannot attach call data without a call-data ID", true, true);
       else if(!key) generateMessage("Cannot attach call data without a key", true, true);
       else if(!value) generateMessage("Cannot attach call data without a value", true, true);
       else {
           makeAuthCall('POST', "/recording/call_data/" + calldataid + "/attached_data", [{'key': key, 'value' : value}]).success(function() {
               printAttachCallData();
           }).fail(function() {
               var error = "Impossible to attach call data";
               generateMessage(error, true, true);
           });
       }
    });

    $('#rs_get_agent_call_history').click(function() {
        var size = $('#rs_ach_size').val();

        var param;
        if(size) param = "?size=" + size;
        else param = "?size=10";
        var agentNum = $('#rs_ach_agent_num').val();

        makeAuthCall('POST', "/recording/history/agent" + param,{"agentNum":agentNum}).success(function(callHistory) {
            printAgentCallHistory(callHistory);
        }).fail(function() {
            var error = "Impossible to retrieve agent call history";
            generateMessage(error, true, true);
        });
    });

    $('#rs_get_user_call_history').click(function() {
        var size = $('#rs_uch_size').val();
        var days = $('#rs_uch_days').val();

        if(size && days) generateMessage("Cannot set size and days at the same time for the user call history", true, true);
        else {
            var param;
            if(!size && !days) param = "?size=10";
            else if(size) param = "?size=" + size;
            else if(days) param = "?days=" + days;
            var _interface = $('#rs_uch_interface').val();

            makeAuthCall('POST', "/recording/history" + param,{"interface":_interface}).success(function(callHistory) {
                printUserCallHistory(callHistory);
            }).fail(function() {
                var error = "Impossible to retrieve user call history";
                generateMessage(error, true, true);
            });
        }
    });

    $('#rs_get_customer_call_history').click(function() {
        var filters = [{"field": $('#rs_cch_field').val(), "operator": $('#rs_cch_operator').val(), "value": $('#rs_cch_value').val()}];
        var size = Number($('#rs_cch_size').val());
        if(!size) size = 7;

        makeAuthCall('POST', "/recording/history/customer",{'filters': filters, 'size' : size}).success(function(callHistory) {
            printCustomerCallHistory(callHistory);
        }).fail(function() {
            var error = "Impossible to retrieve customer call history";
            generateMessage(error, true, true);
        });
    });

    $('#rs_get_last_agent_for_number').click(function() {
        var callerNo = $('#rs_lafn_callerNo').val();
        var since = $('#rs_lafn_since').val();
        if(!callerNo) generateMessage("Cannot get the last agent for a number without a number", true, true);
        else {
            var param = "?callerNo=" + callerNo;
            if(since) param += "&since=" + since;
            makeAuthCall('GET', "/recording/last_agent" + param).success(function(agent) {
                printLastAgentForNumber(agent);
            }).fail(function() {
                var error = "Impossible to retrieve last agent for number";
                generateMessage(error, true, true);
            });
        }
    });
});
