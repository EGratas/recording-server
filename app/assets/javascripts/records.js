var PAGE_SIZE = 50;

$(document).ajaxStart(function() {
    $('#loading').show();
}).ajaxStop(function() {
    $('#loading').hide();
})

angular.module('recording-server', ['Disk'])
.directive('emptyToNull', function () {
    return {
        restrict: 'A',
        require: 'ngModel',
        link: function (scope, elem, attrs, ctrl) {
            ctrl.$parsers.push(function(viewValue) {
                if(viewValue === "") {
                    return null;
                }
                return viewValue;
            });
        }
    };
})
.directive("listenPlayEvent", function($http){
    return function(scope, element, attrs){
        element.bind("play", function(){
            $http.get('records/'+scope.record.id+'/audio/listen');
        });
    }
})
.controller('PageController', function($scope, diskSpace, $log, $http, $window) {
    const calendarLang = document.getElementsByTagName("body")[0].getAttribute("lang") == 'fr' ? 'fr': 'en';

    $scope.checkAlert = function() {
        diskSpace.getAlertMessage().then(function(alertMsg) {
            if (alertMsg !== "") {
                alert(alertMsg);
            } else {
                 $log.info('Disk space checked OK');
            }
        });
    };
    $scope.checkAlert();

    $scope.find = find;
    $scope.request = {
        direction: 'all',
        key: 'recording'
    };
    $scope.search_attached_data = false;

    $scope.processJson = function(json) {
        $scope.records = json.data.records;
        $scope.hasNext = json.data.hasNext;
    }

    $('#start').datetimepicker({
      language: calendarLang,
      defaultDate: moment().subtract(1, 'd')
    });

    $('#end').datetimepicker({
        language: calendarLang
    });

    $('.info-tooltip').tooltip();
    $('#loading').hide();
    find(1);

    function find(pageNum) {
        $scope.currentPage = pageNum
        $scope.fetchFunction = $scope.find
        if($('#start').val() != '')
            $scope.request.start = formatDate($('#start').data("DateTimePicker").getDate());
        else
            delete $scope.request.start;
        if($('#end').val() != '')
            $scope.request.end= formatDate($('#end').data("DateTimePicker").getDate());
        else
            delete $scope.request.end;

        $scope.request.queueCallStatus = 'answered';

        $http.post('records/search?page=' + pageNum + '&pageSize=' + PAGE_SIZE,
          JSON.stringify($scope.request)).then($scope.processJson, function(error) {
          $log.error('Unable to get records ', error);
        });
    }

    function formatDate(date) {
        return date.format('YYYY-MM-DD HH:mm:ss');
    }

    $scope.searchByCallid = function () {
        $http.post('records/callid_search?callid=' + $scope.callid,{}).then($scope.processJson, function(error) {
          $log.error('Unable to get record for callid '+ $scope.callid, error);
        });
    }

    $scope.getAccessLog = function () {
        $window.open('records/access');
    }

    $scope.toggleAttachedDataSearch = function() {
        $scope.search_attached_data = !$scope.search_attached_data;
        if($scope.search_attached_data) {
            $scope.request.key = null;
            $scope.request.value = null
        } else {
            $scope.request.key = 'recording';
            $scope.request.value = null;
        }

    };

    $scope.attachedDataSearchClass = function() {
        var suffix = "plus";
        if($scope.search_attached_data)
            suffix = "minus";
        return "glyphicon glyphicon-" + suffix;
    }
});
