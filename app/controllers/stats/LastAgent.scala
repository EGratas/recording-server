package controllers.stats

import javax.inject.Inject
import models.stats.AgentStats
import play.api.Logger
import play.api.db.{Database, NamedDatabase}
import play.api.libs.json.Json
import play.api.mvc.{AbstractController, ControllerComponents}

class LastAgent @Inject() (
    @NamedDatabase("stats") dbStats: Database,
    cc: ControllerComponents
) extends AbstractController(cc) {
  val logger = Logger(getClass.getName)

  def search(callerNo: String, since: Int) =
    Action { implicit request =>
      {
        dbStats.withConnection(implicit c => {
          val agentNumber = AgentStats.searchLastAgentForCaller(callerNo, since)
          agentNumber match {
            case Some(number) =>
              logger.info(
                s"Return last agent ${number.agentNumber} for caller $callerNo since $since days"
              )
              Ok(Json.toJson(number))
            case None =>
              logger.info(
                s"Last agent for caller $callerNo since $since days not found"
              )
              NotFound
          }
        })
      }
    }

}
