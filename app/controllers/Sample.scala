package controllers

import com.google.inject.Inject
import play.api.Configuration
import play.api.i18n.Lang
import play.api.mvc.{
  AbstractController,
  Action,
  AnyContent,
  ControllerComponents
}

class Sample @Inject() (configuration: Configuration, cc: ControllerComponents)
    extends AbstractController(cc) {
  val lang = Lang("fr")

  def index: Action[AnyContent] =
    Action {
      val authToken = configuration.get[String]("authentication.token")
      Ok(
        views.html.sample.sample("Recording server API Sample Page", authToken)
      )
    }
}
