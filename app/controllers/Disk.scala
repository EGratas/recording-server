package controllers

import configuration.AuthConfig
import models.Partition
import models.authentication.RightsHelper
import play.api.libs.json.Json
import play.api.mvc.{
  AbstractController,
  Call,
  ControllerComponents,
  EssentialAction
}
import play.api.{Configuration, Logger}
import play.api.i18n.I18nSupport
import javax.inject.Inject
import play.api.i18n.Messages

class Disk @Inject() (
    val authConfig: AuthConfig,
    configuration: Configuration,
    val rightsHelper: RightsHelper,
    cc: ControllerComponents,
    secured: Secured
) extends AbstractController(cc)
    with RightsCheck
    with I18nSupport {

  val logger: Logger      = Logger(getClass.getName)
  val alertRatio          = 0.8
  private val audioFolder = configuration.get[String]("audio.folder")

  def page: EssentialAction =
    secured.IsAuthenticated(
      Some(loginRoute),
      implicit request => {
        if (isAdmin())
          Ok(
            views.html
              .disk(rightsHelper)(request.lang, request, request.messages)
          )
        else
          Forbidden(
            views.html
              .forbidden(rightsHelper)(request.lang, request, request.messages)
          )
      }
    )

  def diskUsage(p: Partition): Double = {
    BigDecimal(
      (p.totalSpace.toDouble - p.freeSpace.toDouble) / p.totalSpace.toDouble
    ).setScale(4, BigDecimal.RoundingMode.HALF_UP).toDouble
  }

  def listPartitions: EssentialAction =
    secured.IsAuthenticated(
      None,
      implicit request => {
        val p = Partition
          .forDirectory(audioFolder)
          .copy(name = Messages("RecordingsPartition"))
        val ds = diskUsage(p)

        if (ds >= alertRatio)
          logger.warn(
            s"Recording disk usage is above threshold limit: ${ds * 100}%"
          )
        else logger.info(s"Recording disk usage: ${ds * 100}%")

        Ok(
          Json.toJson(
            List(
              Json.obj(
                "name"       -> p.name,
                "mountPoint" -> p.mountPoint,
                "freeSpace"  -> p.freeSpace,
                "totalSpace" -> p.totalSpace
              )
            )
          )
        )
      }
    )

  val loginRoute: Call = routes.Login.login()

}
