package controllers

import models.authentication.{AdminRight, RightsHelper}
import play.api.mvc.Request

trait RightsCheck {

  val rightsHelper: RightsHelper

  def isAdmin()(implicit r: Request[_]): Boolean = {
    if (r.session.get("isSuperAdmin").isDefined) true
    else {
      r.session.get("username") match {
        case None => false
        case Some(username) =>
          rightsHelper
            .getUserRights(username)
            .orElse(
              Option.when(r.session.get("isSuperAdmin").isDefined)(AdminRight())
            )
            .contains(AdminRight())
      }
    }
  }
}
