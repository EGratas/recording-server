package date_helpers

import com.google.inject.Inject
import org.joda.time.format.DateTimeFormat

class DateHelper @Inject() (val mockableCalendar: MockableCalendar) {

  def dateByNumberOfDays(days: Int): String = {
    val format = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss")

    val today     = mockableCalendar.getCustomTime
    val formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")

    val dtf  = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
    val date = dtf.parseDateTime(today)
    val oldDate =
      formatter.parseDateTime(dtf.print(date.minusDays(days))).toDate

    format.format(oldDate)
  }
}
