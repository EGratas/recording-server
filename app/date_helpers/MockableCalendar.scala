package date_helpers

import java.text.SimpleDateFormat
import java.util.Calendar

import com.google.inject.ImplementedBy

@ImplementedBy(classOf[MockableCalendar])
trait CustomMockableCalendar {
  def getCustomTime: String
}

class MockableCalendar extends CustomMockableCalendar {
  def getCustomTime: String = {
    val format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
    format.format(Calendar.getInstance().getTime)
  }
}
