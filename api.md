# Records

Note: The token used in the X-Auth-Token header in following requests must be the value of the authentication.token key in the application.conf.

## Record list

    curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" 
        -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9400/records/search?pageSize=3  -d '{"agent": "1573"}'
             
    {
        "hasNext": true,
        "records": [
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433866.13971"
                },
                "dst_num": "73555",
                "duration": "00:00:20",
                "id": "xivocc_gateway-1459433866.13971",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:17:46",
                "status": "answered"
            },
            {
                "agent": "Joe Dalton (1573)",
                "attached_data": {
                    "recording": "xivocc_gateway-1459433330.13665"
                },
                "dst_num": "73555",
                "duration": "00:01:01",
                "id": "xivocc_gateway-1459433330.13665",
                "queue": "oneforone (3555)",
                "src_num": "loadtester",
                "start": "2016-03-31 16:08:51",
                "status": "answered"
            }
        ]
    }
                 
### criteria

Examples:

                 -d '{"start":"2016-03-24 15:10:00", "end":"2016-03-25 00:00:00"}'
                 -d '{"queue":"premium"}

Possible values

* agent
* queue
* start
* end
* callee
* caller
* direction
* key
* value

## by call id

        curl -XPOST -H "Accept: application/json" 
            -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  http://192.168.29.101:9400/records/callid_search?callid=1459435466.286075
           
        {
            "hasNext": false,
            "records": [
                {
                    "agent": "Dino Falconetti (1564)",
                    "attached_data": {
                        "recording": "xivocc_gateway-1459435465.15089"
                    },
                    "dst_num": "73556",
                    "duration": "",
                    "id": "xivocc_gateway-1459435465.15089",
                    "queue": "hotline (3556)",
                    "src_num": "loadtester",
                    "start": "2016-03-31 16:44:26",
                    "status": "answered"
                }
            ]
        }
        

## get the audio file

        curl -XGET -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  
            http://192.168.29.101:9400/records/xivocc_gateway-1459435465.15089/audio
            
# Call data

## Attach data to a call

        curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: u@pf#41[gYHJm<]9N[a0iWDQQ7`e9k'  
            http://192.168.29.101:9400/call_data/761054/attached_data  -d '[{"key":"color", "value": "green"}]'

## Historical API

# Customer Call History

Attached data can also be used

        curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: recording'  
            http://192.168.56.1:9400/history/customer  -d '{"filters": [{"field":"src_num", "operator": "=", "value": "1456"}], "size":10}'

        {
        "total":11,
        "list":[
            {"start":"2017-06-13 17:32:45","duration":"00:00:08",
            "wait_time":"00:00:06","agent_name":"Brucé Waill",
            "agent_num":"2500","queue_name":"Bl Record",
            "queue_num":"3012","status":"answered"},
            {"start":"2017-06-13 17:26:54","duration":"00:00:06",
            "wait_time":"00:00:05","agent_name":"Brucé Waill",
            "agent_num":"2500","queue_name":"Blue Ocean",
            "queue_num":"3000","status":"answered"
            }
            ]
        }
       
# Agent Call History

        curl -XPOST -H "Content-Type: application/json" -H "Accept: application/json" -H 'X-Auth-Token: recording-token'  \
            http://localhost:9400/history/agent  -d '{"agentNum": "2500"}'
            
        [
            {"start":"2017-08-30 22:37:36","duration":"00:00:02",
                "src_num":"1168","dst_num":"***301168","status":"abandoned",
                "src_firstname":"Franck","src_lastname":"GOURDEL","dst_firstname":null,"dst_lastname":null},
            {"start":"2017-08-30 22:37:01","duration":"00:00:02",
                "src_num":"1168","dst_num":"***301168","status":"abandoned",
                "src_firstname":"Franck","src_lastname":"GOURDEL","dst_firstname":null,"dst_lastname":null},
         
        ]
       