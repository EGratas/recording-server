module.exports = function(config) {
  config.set({
    basePath: '',
    frameworks: ['jasmine'],

    reporters: ['progress'],
    port: 9876,
    colors: false,
    logLevel: config.LOG_INFO,
    autoWatch: true,
    browsers: ['ChromeHeadless'],
    singleRun: false,
    autoWatchBatchDelay: 300,

    files: [
      './public/javascripts/jquery-2.0.3.min.js',
      './public/javascripts/angular.min.js',
      './public/javascripts/angular-charts.js',
      './public/cust_bootstrap/js/bootstrap.min.js',
      './public/bootstrap/js/moment.js',
      './public/bootstrap/js/bootstrap-datetimepicker.min.js',
      './node_modules/angular-mocks/angular-mocks.js',
      './app/assets/javascripts/disk.js',
      './app/assets/javascripts/recording_control.js',
      './app/assets/javascripts/records.js',
      './app/assets/javascripts/services/diskSpace.service.js',
      './test/karma/jasminecustommatchers.js',
      {pattern: './public/**/*', watched: false, served: true, included: false},
      './test/karma/**/*.spec.js'
    ],

    proxies: {
      "/assets/i18n/": "/base/public/i18n/",
      "/assets/images/": "/base/public/images/"
    }

  });
};
