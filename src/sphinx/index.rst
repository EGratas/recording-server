########################
Serveur d'enregistrement
########################

Le serveur d'enregistrement est un module qui, couplé au service de gestion des droits, permet de rechercher et consulter les enregistrements effectués sur un XiVO.

Gestion des droits
==================

Ce module définit 3 types de droits, qui déterminent quels éléments et quels appels seront visibles dans l'interface d'enregistrement :

+ **Administrateur :** Aucune restriction, tous les éléments et tous les appels sont visibles
+ **Superviseur :** Responsable d'un ou plusieurs groupes d'agents, files d'attentes et appels entrants. Seule la consultation d'enregistrement est disponible, et seuls les appels correspondant aux critères renseignés sont visibles
+ **Formateur :** Dispose des droits de superviseur sur une période de temps limitée

Chaque utilisateur du XiVO possédant un login XiVO Client peut se voir affecter un de ces droits.

La page principale liste les droits existants. Pour créer un nouveau droit pour un utilisateur, entrer une partie de son nom dans la zone de texte à droite, le sélectionner dans le liste déroulante puis cliquer sur |plus.png|.

.. |plus.png| image:: images/plus.png

.. image:: images/selection.png

Sur la page de modifcation, sélectionner le profil voulu, et éventuellement les paramètres additionnels (files d'attente, groupes d'agents et appels entrants pour un superviseur, dates de début et de fin pour un formateur).

.. image:: images/modification.png
    :scale: 75%

Deux types d'utilisateurs peuvent se connecter à l'interface de gestion des droits :

+ le "superadmin" dont le login et le mot de passe sont communiqués à l'installation : lui seul peut créer n'importe quel type de droit.
+ un superviseur déjà configuré : il ne peut créer que des formateurs avec des droits équivalents ou inférieurs à ceux dont il dispose lui-même. Un superviseur doit utiliser son nom d'utilisateur et son mot de passe XiVO Client pour se connecter.

Interface d'enregistrement
==========================

Ce module permet de :

+ Rechercher et télécharger des enregistrements ("Liste des enregistrements")
+ Consulter l'espace disque disponible sur le serveur ("Gestion des disques")
+ Configurer des numéros internes et des SDA qui ne doivent pas être enregistrés ("Contrôle d'enregistrement")

Seuls les utilisateurs configurés dans le module de gestion des droits peuvent accéder à cette interface, en utilisant leur nom d'utilisateur et leur mot de passe XiVO Client. Les superviseurs et les formateurs ne verront que les enregistrements des files d'attente, appels entrants ou agents dont ils ont la charge. De plus, il n'auront pas accès à la gestion des disques et au contrôle d'enregistrement.

+ **Liste des enregistrements**

|
.. image:: images/recherche.png

+ **Gestion des disques**

.. image:: images/disques.png

+ **Contrôle d'enregistrement**

.. image:: images/controle.png

