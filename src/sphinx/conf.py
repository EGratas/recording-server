# -*- coding: utf-8 -*-

# Copyright (C) 2013-2014 Avencall
#
import sys, os

extensions = []
templates_path = [u'_templates']
source_suffix = '.rst'
source_encoding = u'utf-8-sig'
master_doc = u'index'
project = u'Serveur d\'enregistrement'
copyright = u'2014-2015, Avencall'
version = release = u'2.4.31'
language = u'fr'
exclude_patterns = []
pygments_style = 'sphinx'
html_theme = u'nature'
html_static_path = ['_static']

todo_include_todos = True

pdf_documents = [ ('index', u'Serveur d\'enregistrement', u'Avencall') ]
pdf_language = "fr_FR"
pdf_break_level = 1
pdf_use_toc = True
pdf_toc_depth = 9999
pdf_use_numbered_links = False
pdf_fit_background_mode = 'scale'
