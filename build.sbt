import com.typesafe.sbt.packager.docker._
import com.typesafe.sbt.web.{CompileProblemsException, GeneralProblem}

import scala.sys.process._

val appName = "recording-server"
// Do not change version directly but change TARGET_VERSION file and run `xivocc-build.sh apply-version`
val appVersion = sys.env.getOrElse("TARGET_VERSION", "dev-version")
val appOrganisation = "xivo"

lazy val main = Project(appName, file("."))
  .enablePlugins(PlayScala, DockerPlugin, BuildInfoPlugin)
  .settings(
    name := appName,
    version := appVersion,
    scalaVersion := Dependencies.scalaVersion,
    semanticdbEnabled := true,
    semanticdbVersion := scalafixSemanticdb.revision,
    organization := appOrganisation,
    resolvers ++= Dependencies.resolutionRepos,
    libraryDependencies ++= Dependencies.runDep,
    routesGenerator := InjectedRoutesGenerator,
    pipelineStages := Seq(digest),
    dist := (dist dependsOn npmTest).value,
    (stage in Docker) := (stage in Docker).value,
    testOptions in Test += Tests.Argument("-oF")
  )
  .settings(scalaCompilerSettings: _*)
  .settings(testSettings: _*)
  .settings(javaScriptSettings: _*)
  .settings(
    setVersionVarTask := {
      System.setProperty("APPLI_VERSION", appVersion)
    },
    edit in EditSource := ((edit in EditSource) dependsOn (EditSourcePlugin.autoImport.clean in EditSource)).value,
    packageBin in Compile := ((packageBin in Compile) dependsOn (edit in EditSource)).value,
    run in Compile := ((run in Compile) dependsOn setVersionVarTask).evaluated
  )
  .settings(dockerSettings: _*)
  .settings(editSourceSettings: _*)
  .settings(buildInfoSettings: _*)
  .settings(docSettings: _*)
  .settings(
    coverageExcludedPackages := "<empty>;views\\..*;Reverse.*"
  )
  .settings(
    javaOptions in Test ++= scala.jdk.CollectionConverters.propertiesAsScalaMap(System.getProperties)
      .map { case (key, value) => "-D" + key + "=" + value }.toSeq
  )

lazy val setVersionVarTask = taskKey[Unit]("Set version to a env var")
lazy val npmTest = taskKey[Unit]("Run NPM headless test")


lazy val buildInfoSettings = Seq(
  buildInfoKeys := Seq[BuildInfoKey](name, version, scalaVersion, sbtVersion),
  buildInfoPackage := "server.info"
)

lazy val testSettings = Seq(
  parallelExecution in Test := false,
  parallelExecution in Test := false,
  (test in Test) := ((test in Test) dependsOn npmTest).value,
  javaOptions in Test += "-Dconfig.file=test/resources/application.conf",
  javaOptions in Test += "-Dlogger.file=test/resources/logback-test.xml"
)

lazy val javaScriptSettings = Seq(
  npmTest := {
    val log = new CustomLogger
    if ("npm ci".!(log) != 0) throw new CompileProblemsException(Array(new GeneralProblem(log.buf.toString, file("./package.json"))))
    if ("npm run test-headless".!(log) != 0) throw new RuntimeException("NPM Tests failed")
  }
)

lazy val scalaCompilerSettings = Seq(
  scalaVersion := Dependencies.scalaVersion,
  scalacOptions ++= Seq(
    "-encoding", "UTF-8",
    "-unchecked",
    "-deprecation",
    "-feature",
    "-language:existentials",
    "-language:higherKinds",
    "-language:implicitConversions",
    "-Xlint:adapted-args",
    "-Ymacro-annotations",
    "-Ywarn-dead-code",
    "-Wunused:imports",
    s"-Wconf:src=${target.value}/.*:s"
  )
)

lazy val dockerSettings = Seq(
  maintainer in Docker := "R&D <randd@xivo.solutions>",
  dockerBaseImage := "openjdk:8u275-jdk-slim-buster",
  dockerExposedPorts := Seq(9000),
  dockerRepository := Some("xivoxc"),
  dockerCommands += Cmd("LABEL", s"""version="$appVersion""""),
  dockerEntrypoint := Seq("bin/recording-server-docker"),
  daemonUserUid in Docker := None,
  daemonUser in Docker := "daemon",
  dockerChmodType := DockerChmodType.UserGroupWriteExecute
)


lazy val editSourceSettings = Seq(
  flatten in EditSource := true,
  mappings in Universal += file("target/version/appli.version") -> "conf/appli.version",
  targetDirectory in EditSource := baseDirectory.value / "target/version",
  variables in EditSource += ("SBT_EDIT_APP_VERSION", appVersion),
  (sources in EditSource) ++= (baseDirectory.value / "src/res" * "appli.version").get
)

lazy val docSettings = Seq(
  publishArtifact in(Compile, packageDoc) := false,
  sources in(Compile, doc) := Seq.empty
)
